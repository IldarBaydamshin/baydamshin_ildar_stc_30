/*
Урок 2
Задание 2.
Реализовать приложение, которое выполняет разворот массива (массив вводится с клавиатуры).
class Program {
	public static void main(String args[]) {
	Scanner scanner = new Scanner(System.in);
	int n = scanner.nextInt();
	int array[] = new int[n];
	...
	System.out.println(Arrays.toString(array)); // выводится массивв зеркальном представлении
	}
}
*/

import java.util.Scanner;
import java.util.Arrays;

public class task02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        for (int i = n - 1; i > -1; i--) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
    }
}
