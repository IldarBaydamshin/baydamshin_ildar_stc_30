/*
Урок 2
Задание 5 *.
Реализовать приложение, которое выполняет сортировку массива методом пузырька.
*/

import java.util.Scanner;
import java.util.Arrays;

public class task05 {
    public static void main(String[] args) {

        // Вводим массив с клавиатуры
        Scanner scanner = new Scanner(System.in);
        int arraySize = scanner.nextInt();
        int[] array = new int[arraySize];
        int currentArrayIndex = 0;
        while (currentArrayIndex < arraySize) {
            array[currentArrayIndex] = scanner.nextInt();
            currentArrayIndex++;
        }

        // сортировка массива методом пузырька
        for (int leftIndex = 0; leftIndex < array.length; leftIndex++) {
            for (int rightIndex = array.length - 1; rightIndex > leftIndex; rightIndex--) {
                if (array[rightIndex] < array[rightIndex - 1]) {
                    array[rightIndex] = array[rightIndex] + array[rightIndex - 1];
                    array[rightIndex - 1] = array[rightIndex] - array[rightIndex - 1];
                    array[rightIndex] = array[rightIndex] - array[rightIndex - 1];
                }
            }
        }

        // вывод отсортированногоо массива
        System.out.println(Arrays.toString(array));
    }
}
