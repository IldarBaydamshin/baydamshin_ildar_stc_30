/*
Урок 2
Задание 4.
Реализовать приложение, которое меняет местами максимальный и минимальный элементы массива 
(массив вводится с клавиатуры).
*/

import java.util.Scanner;
import java.util.Arrays;

public class task04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];

        if (n > 0) {
            array[0] = scanner.nextInt();
            int maxValue = array[0];
            int maxIndex = 0;
            int minValue = array[0];
            int minIndex = 0;

            for (int i = 1; i < n; i++) {
                array[i] = scanner.nextInt();
                if (array[i] > maxValue) {
                    maxValue = array[i];
                    maxIndex = i;
                } else if (array[i] < minValue) {
                    minValue = array[i];
                    minIndex = i;
                }
            }
            array[minIndex] = maxValue;
            array[maxIndex] = minValue;
            System.out.println(Arrays.toString(array));
        }
    }
}