/**
 * Урок 2
 * Задание 7 *.
 * Реализовать приложение, которое заполняет двумерный массив M*N последовательностью чисел “по спирали”.
 * <p>
 * Например, для массива 3 на 3:
 * <p>
 * 123
 * 894
 * 765
 **/

import java.util.Scanner;
import java.util.Arrays;

public class task07 {
    public static void main(String[] args) {

        // Вводим массив с клавиатуры
        Scanner scanner = new Scanner(System.in);
        int M = scanner.nextInt();
        int N = scanner.nextInt();
        int[][] array = new int[M][N];
        int currentLine = 0, currentColumn = 0;
        int left = 0, right = N - 1, up = 0, down = M - 1;
        String direction = "right";// down, left, up

        for (int v = 1; v <= M * N; v++) {
            if (direction == "right") {
                array[currentLine][currentColumn] = v;
                if (currentColumn != right) {
                    currentColumn++;
                } else {
                    direction = "down";
                    up++;
                    currentLine++;
                }
            } else if (direction == "down") {
                array[currentLine][currentColumn] = v;
                if (currentLine != down) {
                    currentLine++;
                } else {
                    direction = "left";
                    right--;
                    currentColumn--;
                }
            } else if (direction == "left") {
                array[currentLine][currentColumn] = v;
                if (currentColumn != left) {
                    currentColumn--;
                } else {
                    direction = "up";
                    down--;
                    currentLine--;
                }
            } else if (direction == "up") {
                array[currentLine][currentColumn] = v;
                if (currentLine != up) {
                    currentLine--;
                } else {
                    direction = "right";
                    left++;
                    currentColumn++;
                }
            }
        }

        System.out.println();
        for (int i = 0; i != array.length; i++) {
            System.out.println(Arrays.toString(array[i]));
        }
    }
}
