/**
 * Урок 2
 * Задание 6 *.
 * Реализовать приложение, которое выполняет преобразование массива в число.
 **/

public class task06 {
    public static void main(String[] args) {
        int[] array = {4, 2, 3, 5, 7};
        int number = 0;
        for (int i = 0; i != array.length; i++) {
            number += array[i] * Math.pow(10, array.length - 1 - i);
        }
        System.out.println(number); // программа должна вывести 42357
    }
}
