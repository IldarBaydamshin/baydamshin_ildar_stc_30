/*
Урок 2
Задание 3.
Реализовать приложение, которое вычисляет среднее арифметическое элементов массива 
(массив вводится с клавиатуры).

class Program {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in); int n = scanner.nextInt();
		int array[] = new int[n];
		int arrayAverage = 0;
		...
		System.out.println(arrayAverage); 
		}
}
*/

import java.util.Scanner;

public class task03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        float arrayAverage = 0; //Заменил на float для более точного результата
        float arraySum = 0; //Заменил на float для более точного результата

        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
            arraySum += array[i];
        }

        arrayAverage = arraySum / n;
        System.out.printf("%.2f\n", arrayAverage);
        // %.2f\n  до сотых и переводит курсор на следующую строку
    }
}
