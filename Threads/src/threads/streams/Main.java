package threads.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();

        Random random = new Random();
        int max = 10000000;

        for (int i = 0; i < max; i++) {
            numbers.add(random.nextInt(max));
        }

        numbers.parallelStream().filter(number -> {
            if (number == 2 || number == 3) {
                return true;
            }

            for (int i = 2; i * i < number; i++) {
                if (number % i == 0) {
                    return false;
                }
            }
            return true;
        }).forEach(System.out::println);
    }
}

