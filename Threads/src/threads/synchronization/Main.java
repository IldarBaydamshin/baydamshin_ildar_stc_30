package threads.synchronization;

public class Main {
    public static void main(String[] args) {
//        CreditCart visa = new CreditCart(1000);
//        HumanWithMutex husband = new HumanWithMutex("Home", visa);
//        HumanWithMutex wife = new HumanWithMutex("Fame", visa);

//        husband.start();
//        wife.start();

        CreditCart masterCart = new CreditCart(1000);
        HumanWithLock boy = new HumanWithLock("Саша ", masterCart);
        HumanWithLock girl = new HumanWithLock("Таня ", masterCart);

        boy.start();
        girl.start();
    }

}
