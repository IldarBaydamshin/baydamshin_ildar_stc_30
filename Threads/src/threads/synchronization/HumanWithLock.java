package threads.synchronization;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class HumanWithLock extends Thread {
    private String name;
    private final CreditCart creditCart;
    private static final Lock lock = new ReentrantLock();


    public HumanWithLock(String name, CreditCart creditCart) {
        this.name = name;
        this.creditCart = creditCart;


    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            lock.lock();

            if (creditCart.getAmount() > 0) {
                System.out.println(name + " хочет купить ");

                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }

                if (creditCart.buy(10)) {
                    System.out.println(name + " купил ");
                } else {
                    System.out.println(name + " ..... Oops ...");
                }
            }

            lock.unlock();
        }
    }
}
