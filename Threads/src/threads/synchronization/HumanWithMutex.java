package threads.synchronization;

public class HumanWithMutex extends Thread{
    private String name;
    private final CreditCart creditCart;

    public HumanWithMutex(String name, CreditCart creditCart) {
        this.name = name;
        this.creditCart = creditCart;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            synchronized (creditCart) {
                if (creditCart.getAmount() > 0 ) {
                    System.out.println(name + " хочет купить ");

                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }

                    if (creditCart.buy(10)) {
                        System.out.println( name + " купил ");
                    } else {
                        System.out.println(name + " ..... Oops ...");
                    }
                }
            }
        }
    }
}
