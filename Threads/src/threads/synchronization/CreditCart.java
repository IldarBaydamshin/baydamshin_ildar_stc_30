package threads.synchronization;

public class CreditCart {
     private int amount;

    public CreditCart(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public boolean buy(int cost) {
        if (cost <= amount) {
            this.amount -= cost;
            return true;
        } else {
            System.out.println(" А денег нет!!! ");
            return false;
        }
    }
}
