package threads.service;

public class Main {
    public static void main(String[] args) {
        ThreadService threadService = new ThreadService();
        threadService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.print("Hello! ");
            }
        });

        threadService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Ildar! ");
            }
        });

        threadService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(" ok ");
            }
        });

    }
}
