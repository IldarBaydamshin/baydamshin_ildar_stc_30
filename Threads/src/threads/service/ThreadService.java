package threads.service;

public class ThreadService {
    public void submit(Runnable task) {
        Thread newThread = new Thread(task);
        newThread.start();

    }
}
