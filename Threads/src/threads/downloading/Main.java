package threads.downloading;

import threads.service.ThreadService;

import java.io.*;
import java.net.URL;
import java.util.UUID;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        File file = new File("links.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));

//        ThreadService threadService = new ThreadService();
//        String lineFromFile = reader.readLine();
//        while ((lineFromFile != null)) {
//            final String _file = lineFromFile;
//            threadService.submit(() -> {
//                try {
//                    saveFile(_file);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            });
//            saveFile(lineFromFile);
//            lineFromFile = reader.readLine();
//        }



//          reader.lines().collect(Collectors.toList()).parallelStream()
//                  .forEach(fileName -> {
//                      try {
//                          saveFile(fileName);
//                      } catch (IOException e) {
//                          e.printStackTrace();
//                      }
//                  });

    }

    public static void saveFile(String fileName) throws IOException {
        URL url = new URL(fileName);
        InputStream in = new BufferedInputStream(url.openStream());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int n = 0;
        while (-1 != (n = in.read(buf))) {
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        byte[] response = out.toByteArray();

        String newFileName = UUID.randomUUID().toString() + ".png";
        FileOutputStream fos = new FileOutputStream("images/" + UUID.randomUUID().toString() + ".png");
        fos.write(response);
        fos.close();
        System.out.println("FILE SAVED - " + newFileName);

    }

}
