package threads.base;

import java.util.Random;

public class Tirex implements Runnable{
    private Random random = new Random();

    @Override
    public void run() {
        for (int i = 0; i < 1000000; i++) {
            if (i % 10000 == 0) {
                try {
                    Thread.sleep(random.nextInt(2000));
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
            System.out.println(Thread.currentThread().getName() + " Tirex");
        }
    }
}
