package threads.base;

import threads.base.EggTread;
import threads.base.HenThread;
import threads.base.Tirex;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        EggTread eggTread = new EggTread();
        HenThread henThread = new HenThread();
        eggTread.start();
        henThread.start();

        Tirex tirex = new Tirex();


        eggTread.join();
        henThread.join();
        new Thread(tirex).start();

    }
}
