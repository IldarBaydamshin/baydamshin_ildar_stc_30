/*
    2.  Реализовать Adjacency List для графа, а также bfs и dfs.
*/

import graph.*;

public class MainForGraph {
    public static void main(String[] args) {
        Vertex a0 = new VertexIntegerImpl(0);
        Vertex a1 = new VertexIntegerImpl(1);
        Vertex a2 = new VertexIntegerImpl(2);
        Vertex a3 = new VertexIntegerImpl(3);
        Vertex a4 = new VertexIntegerImpl(4);
        Vertex a5 = new VertexIntegerImpl(5);
        Vertex a6 = new VertexIntegerImpl(6);
        Vertex a7 = new VertexIntegerImpl(7);
        Vertex a8 = new VertexIntegerImpl(8);
        Vertex a9 = new VertexIntegerImpl(9);

        Edge edge0 = new EdgeTwoVerticesImpl(a0, a1, 3);
        Edge edge1 = new EdgeTwoVerticesImpl(a0, a3, 7);
        Edge edge2 = new EdgeTwoVerticesImpl(a0, a4, 8);
        Edge edge3 = new EdgeTwoVerticesImpl(a1, a2, 1);
        Edge edge4 = new EdgeTwoVerticesImpl(a1, a3, 4);
        Edge edge5 = new EdgeTwoVerticesImpl(a4, a3, 3);
        Edge edge6 = new EdgeTwoVerticesImpl(a3, a9, 2);
        Edge edge7 = new EdgeTwoVerticesImpl(a4, a5, 3);
        Edge edge8 = new EdgeTwoVerticesImpl(a3, a5, 1);
        Edge edge9 = new EdgeTwoVerticesImpl(a3, a6, 5);
        Edge edge10 = new EdgeTwoVerticesImpl(a2, a7, 3);
        Edge edge11 = new EdgeTwoVerticesImpl(a9, a7, 5);
        Edge edge12 = new EdgeTwoVerticesImpl(a7, a9, 7);
        Edge edge13 = new EdgeTwoVerticesImpl(a5, a8, 2);
        Edge edge14 = new EdgeTwoVerticesImpl(a6, a8, 3);
        Edge edge15 = new EdgeTwoVerticesImpl(a3, a9, 5);
        Edge edge16 = new EdgeTwoVerticesImpl(a8, a9, 5);

        OrientedGraph adjacencyList = new GraphAdjacencyListImpl();
        adjacencyList.addEdge(edge0);
        adjacencyList.addEdge(edge1);
        adjacencyList.addEdge(edge2);
        adjacencyList.addEdge(edge3);
        adjacencyList.addEdge(edge4);
        adjacencyList.addEdge(edge5);
        adjacencyList.addEdge(edge6);
        adjacencyList.addEdge(edge7);
        adjacencyList.addEdge(edge8);
        adjacencyList.addEdge(edge9);
        adjacencyList.addEdge(edge10);
        adjacencyList.addEdge(edge11);
        adjacencyList.addEdge(edge12);
        adjacencyList.addEdge(edge13);
        adjacencyList.addEdge(edge14);
        adjacencyList.addEdge(edge15);
        adjacencyList.addEdge(edge16);

        adjacencyList.print();
        System.out.println();

        adjacencyList.bfsPrint();
        System.out.println();
        adjacencyList.dfsPrint();
        System.out.println();
    }
}
