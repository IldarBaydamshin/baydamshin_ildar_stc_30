package graph;

import java.util.*;

public class GraphAdjacencyListImpl implements OrientedGraph {
    private static final int MAX_VERTICES_COUNT = 10;

    static class Node {
        Vertex vertex;
        LinkedList<Edge> edges;
        int layer = - 1;
        boolean isVisited = false;

        public Node(Vertex vertex, LinkedList<Edge> edges) {
            this.vertex = vertex;
            this.edges = edges;
        }
    }

    private final Node[] nodes = new Node[MAX_VERTICES_COUNT];

    @Override
    public void addEdge(Edge edge) {
        if (nodes[edge.getFirst().getNumber()] == null) {
            nodes[edge.getFirst().getNumber()] = new Node(edge.getFirst(), new LinkedList<>());
            nodes[edge.getFirst().getNumber()].edges.add(edge);
        } else {
            nodes[edge.getFirst().getNumber()].edges.add(edge);
        }
    }

    @Override
    public void print() {
        System.out.println("Vertex -> Vertex(Weight)");
        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i] != null) {
                System.out.print(nodes[i].vertex.getNumber() + " ->  ");
                if (!nodes[i].edges.isEmpty()) {
                    for (Edge e : nodes[i].edges) {
                        System.out.print(e.getSecond().getNumber() + "(" + e.weight() + ");   ");
                    }
                }
                System.out.println();
            }
        }
        System.out.println();
    }

    @Override
    public void bfsPrint() {
        Deque<Node> deque = new LinkedList<>();
        nodes[0].layer = 0;
        nodes[0].isVisited = true;
        deque.add(nodes[0]);
        Node currentNode;

        System.out.println("Print BFS:");
        int lastLayer = 0;
        while (!deque.isEmpty()) {
            currentNode = deque.pop();
            if (currentNode.isVisited) {
                if (currentNode.layer == lastLayer) {
                    System.out.print(currentNode.vertex.getNumber() + " ");
                } else {
                    System.out.print("\n" + currentNode.vertex.getNumber() + " ");
                    lastLayer++;
                }
            }
            for (Edge edge : currentNode.edges) {
                if (nodes[edge.getSecond().getNumber()] != null) {
                    if (!nodes[edge.getSecond().getNumber()].isVisited) {
                        nodes[edge.getSecond().getNumber()].layer = lastLayer + 1;
                        nodes[edge.getSecond().getNumber()].isVisited = true;
                        deque.add(nodes[edge.getSecond().getNumber()]);
                    }
                }
            }
        }
        System.out.println();
    }

    @Override
    public void dfsPrint() {
        for (int i = 0; i <MAX_VERTICES_COUNT; i++) {
            nodes[i].layer = 0;
            nodes[i].isVisited = false;
        }

        Deque<Node> stack = new LinkedList<>();
        nodes[0].layer = 0;
        nodes[0].isVisited = true;
        stack.add(nodes[0]);
        Node currentNode;
        System.out.println("Print DFS:");
        while (!stack.isEmpty()) {
            currentNode = stack.poll();


            for (int i = currentNode.edges.size() - 1; i >=0; i--) {
                if (nodes[currentNode.edges.get(i).getSecond().getNumber()] != null) {
                    if (!nodes[currentNode.edges.get(i).getSecond().getNumber()].isVisited) {
                        nodes[currentNode.edges.get(i).getSecond().getNumber()].isVisited = true;
                        stack.push(nodes[currentNode.edges.get(i).getSecond().getNumber()]);
                    }
                }
            }

            if (currentNode.isVisited) {
                if (!stack.isEmpty()) {
                    System.out.print(currentNode.vertex.getNumber() + " -> ");
                } else {
                    System.out.println(currentNode.vertex.getNumber());
                }
            }
        }
    }
}

