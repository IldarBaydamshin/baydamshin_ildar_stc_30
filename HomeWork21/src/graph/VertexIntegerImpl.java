package graph;

import java.util.Objects;

public class VertexIntegerImpl implements Vertex {

    private int number;

    public VertexIntegerImpl(int number) {
        this.number = number;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VertexIntegerImpl that = (VertexIntegerImpl) o;
        return number == that.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
