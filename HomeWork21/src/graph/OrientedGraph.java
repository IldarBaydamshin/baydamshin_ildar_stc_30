package graph;

public interface OrientedGraph {
    void addEdge(Edge edge);
    void print();
    void bfsPrint();
    void dfsPrint();
}
