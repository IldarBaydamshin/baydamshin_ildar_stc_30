package bst;

import java.util.*;

public class BinarySearchTreeImpl<T extends Comparable<T>> implements BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?> node = (Node<?>) o;
            return Objects.equals(value, node.value) && Objects.equals(left, node.left) && Objects.equals(right, node.right);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value, left, right);
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        } else if (value.compareTo(root.value) < 0) {
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }
        return root;
    }


    @Override
    public void printDfsByQueue() {
        dfs(root);
    }

    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);

        Node<T> current;

        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.print(current.value + " ");
            if (current.left != null) {
                stack.push(current.left);
            }
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    private void dfs(Node<T> root) {
        if (root != null) {
            dfs(root.left);
            System.out.print(root.value + " ");
            dfs(root.right);
        }
    }

    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);

        Node<T> current;

        while (!queue.isEmpty()) {
            current = queue.poll();
            System.out.print(current.value + " ");
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }

        }
    }

    @Override
    public void printBfsByLayers() {

        class Element {
            int level;
            Node<T> node;

            public Element(int level, Node<T> node) {
                this.level = level;
                this.node = node;
            }
        }

        Deque<Element> elements = new LinkedList<>();

        elements.add(new Element(1, root));
        Element currentElement;

        int lastLevel = 1;
        while (!elements.isEmpty()) {
            currentElement = elements.poll();

            if (currentElement.level == lastLevel) {
                System.out.print(currentElement.node.value + " ");
            } else {
                System.out.print("\n" + currentElement.node.value + " ");
                lastLevel++;
            }


            if (currentElement.node.left != null) {
                elements.add(new Element(currentElement.level + 1, currentElement.node.left));
            }

            if (currentElement.node.right != null) {
                elements.add(new Element(currentElement.level + 1, currentElement.node.right));
            }
        }
        System.out.println();;

    }

    @Override
    public void remove(T valueToRemove) {
        Node<T> currentParent = new Node<>(root.value);
        Node<T> currentNode = root;
        currentParent.right = currentNode;
        boolean isLeaf;
        boolean hasLeft;
        boolean hasRight;
        boolean hasOneChild;

        while (currentNode != null) {
            if (valueToRemove.compareTo(currentNode.value) == 0) {
                Node<T> parentOfNodeToRemove = currentParent;
                Node<T> nodeToRemove = currentNode;


                isLeaf = nodeToRemove.left == null && nodeToRemove.right == null;
                hasLeft = currentNode.left != null;
                hasRight = currentNode.right != null;
                hasOneChild = (hasLeft ^ hasRight);

                // удалеем если лист
                if (isLeaf) {
                    if (parentOfNodeToRemove.value.compareTo(nodeToRemove.value) > 0) {
                        parentOfNodeToRemove.left = null;
                    } else {
                        parentOfNodeToRemove.right = null;
                    }
                    break;
                }

                //удаляем если один потомок
                if (hasOneChild) {
                    if (hasLeft) {
                        if (parentOfNodeToRemove.value.compareTo(nodeToRemove.value) > 0) {
                            parentOfNodeToRemove.left = nodeToRemove.left;
                        } else {
                            parentOfNodeToRemove.right = nodeToRemove.left;
                        }
                    } else {
                        if (parentOfNodeToRemove.value.compareTo(nodeToRemove.value) > 0) {
                            parentOfNodeToRemove.left = nodeToRemove.right;
                        } else {
                            parentOfNodeToRemove.right = nodeToRemove.right;
                        }
                    }
                    break;
                }


                //удаление узла с двумя потомками

                //находим узел левого поддерева без правого потомка
                currentParent = currentNode;
                currentNode = currentNode.left;
                hasRight = currentNode.right != null;
                while (hasRight) {
                    currentParent = currentNode;
                    currentNode = currentParent.right;
                    hasRight = currentNode.right != null;
                }

                Node<T> parentOfNodeToReplace = currentParent;
                Node<T> nodeToReplace = currentNode;

                // Удаление узла если он корень
                if (nodeToRemove.equals(root)) {
                    if (parentOfNodeToReplace.value.compareTo(nodeToReplace.value) < 0) {
                        parentOfNodeToReplace.right = null;
                    } else {
                        parentOfNodeToReplace.left = null;
                    }
                    nodeToReplace.right = root.right;
                    nodeToReplace.left = root.left;
                    root = nodeToReplace;
                    break;
                }

                // удаление узла если его замена левый потомок
                if (nodeToRemove.left.equals(nodeToReplace)) {
                    nodeToReplace.right = nodeToRemove.right;
                    if (parentOfNodeToRemove.value.compareTo(nodeToRemove.value) > 0) {
                        parentOfNodeToRemove.left = nodeToReplace;
                    } else {
                        parentOfNodeToRemove.right = nodeToReplace;
                    }
                    break;
                }

                nodeToReplace.left = nodeToRemove.left;
                nodeToReplace.right = nodeToRemove.right;

                if (parentOfNodeToReplace.value.compareTo(nodeToReplace.value) < 0) {
                    parentOfNodeToReplace.left = null;
                } else {
                    parentOfNodeToReplace.right = null;
                }
                nodeToRemove.left = null;
                break;
            }
            currentParent = currentNode;
            currentNode = currentNode.value.compareTo(valueToRemove) < 0 ? currentNode.right : currentNode.left;
        }
    }

    @Override
    public boolean contains(T value) {
        return checkNode(root, value);
    }

    private boolean checkNode(Node<T> node, T value) {
        if (value.compareTo(node.value) == 0) {
            return true;
        } else if (value.compareTo(node.value) < 0 && node.left != null) {
            return checkNode(node.left, value);
        } else if (value.compareTo(node.value) > 0 && node.right != null) {
            return checkNode(node.right, value);
        }
        return false;
    }
}
