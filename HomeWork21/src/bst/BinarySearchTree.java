package bst;

public interface BinarySearchTree<T extends Comparable<T>> {
    void insert(T value);
    void printDfsByQueue();
    void printDfsByStack();
    // TODO: выводить по-уровням
    void printBfs();
    void printBfsByLayers();

    // TODO:
    void remove(T value);
    boolean contains(T value);
}
