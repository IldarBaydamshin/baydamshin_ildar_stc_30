/*
 1.  Реализовать функцию удаления значения из дерева (remove),
     реализовать функцию поиска значения в дереве (contains),
     реализовать “поуровневый” вывод в bfs (каждый уровень дерева с новой строки).
     см. TODO в исходном коде занятия.


*/


import bst.BinarySearchTree;
import bst.BinarySearchTreeImpl;

public class MainForBST {

    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTreeImpl<>();
        tree.insert(8);
        tree.insert(3);
        tree.insert(10);
        tree.insert(1);
        tree.insert(6);
        tree.insert(14);
        tree.insert(4);
        tree.insert(7);
        tree.insert(13);

        int value;
        value = 8;

        System.out.println("tree.contains(" + value + ") = " + tree.contains(value));
        tree.printBfsByLayers();
        tree.remove(value);
        System.out.println("tree.contains(" + value + ") = " + tree.contains(value));
        tree.printBfsByLayers();






    }
}
