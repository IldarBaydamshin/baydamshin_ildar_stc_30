/*
IV. Библиотека классов Java
    No п/п 4.4
    Подготовить файл с записями, имеющими следующую структуру:
    [НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
        Используя Java Stream API, вывести:
        1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
        2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
        3) Вывести цвет автомобиля с минимальной стоимостью.
        4) Среднюю стоимость Camry.
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        File file = new File("cars.txt");

        //1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
        String color = "черный";
        int run = 0; //Пробег автомобиля
        System.out.println(foo(file, color, run));

        // 2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
        int minPrice = 700000;
        int maxPrice = 800000;
        System.out.println(uniqModelsInPriceRange(file, minPrice, maxPrice));

        // 3) Вывести цвет автомобиля с минимальной стоимостью.
        System.out.println(color(file));

        //4) Среднюю стоимость Camry.
        String model = "Camry";
        System.out.println(modelAveragePrice(model, file));
    }

    public static Set<String> foo(File file, String color, int run) {
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.map(line -> line.split(";"))
                .filter(line -> (line[2].equalsIgnoreCase(color)
                        || Integer.parseInt(line[3]) == run))
                .map(strings -> strings[0])
                .collect(Collectors.toSet());
    }

    public static int uniqModelsInPriceRange(File file, int minPrice, int maxPrice) {
        Stream<String> stream = null;
        HashMap<String, Integer> hashMap = new HashMap();

        try {
            stream = Files.lines(Paths.get(file.getAbsolutePath()));
            stream.map(line -> (line.split(";")))
                    .filter(line -> (Integer.parseInt(line[4])) >= minPrice)
                    .filter(line -> (Integer.parseInt(line[4])) <= maxPrice)
                    .map(line -> line[1])
                    .forEach(line -> hashMap.put(line, hashMap.get(line) == null ? 1 : hashMap.get(line) + 1));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  (int) hashMap.entrySet().stream().filter(entry -> entry.getValue() == 1).count();
    }

    public static String color(File file) {
        Stream<String> stream = null;
        String color = "";
        try {
            stream = Files.lines(Paths.get(file.getAbsolutePath()));
            int minPrice = stream.map(line -> line.split(";"))
                    .mapToInt(num -> Integer.parseInt(num[4]))
                    .min().getAsInt();

            stream = Files.lines(Paths.get(file.getAbsolutePath()));
            color = stream.map(line -> line.split(";"))
                    .filter(line -> Integer.parseInt(line[4]) == minPrice)
                    .map(strings -> strings[2])
                    .distinct()
                    .collect(Collectors.joining(", "));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return color;
    }

    public static int modelAveragePrice(String modelOfCar, File file) {
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (int) stream.map(line -> line.split(";"))
                .filter(strings -> strings[1].contains(modelOfCar))
                .mapToInt(value -> Integer.parseInt(value[4]))
                .average().getAsDouble();
    }
}





