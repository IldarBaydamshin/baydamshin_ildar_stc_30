/*
Реализовать приложение, рассчитывающее определенный интеграл методом Симпсона по формуле:
см. файл "Занятие_03.pdf"
Под знаком суммы показан шаг цикла, равный двум.
Рассмотреть случаи для разных разбиений.
*/

public class task02 {
    public static void main(String[] args) {
        double x1 = 0;
        double x2 = 12;
        int[] numOfSplits = {10, 100, 1_000, 10_000, 100_000, 1_000_000};

        integralWithSeveralIntervals(x1, x2, numOfSplits);
    }

    public static double f(double x) {
        return Math.pow(x, 2);
    }

    public static double integralBySimpson(double a, double b, int n) {
        double h = (b - a) / n;
        double sum = 0;

        for (double i = a + h; i < b; i += h * 2) {
            sum += f(i - h) + 4 * f(i) + f(i + h);
        }
        return sum * h / 3;
    }

    public static void integralWithSeveralIntervals(double a, double b, int[] array) {
        for (int i : array) {
            System.out.println(integralBySimpson(a, b, i));
        }
    }
}
