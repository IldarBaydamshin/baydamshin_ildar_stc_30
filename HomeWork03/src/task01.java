/**
 * Урок 3
 * Задание 1.
 * <p>
 * Реализовать приложение, со следующим набором функций и процедур:
 * + выводит сумму элементов массива
 * + выполняет разворот массива (массив вводится с клавиатуры).
 * + вычисляет среднее арифметическое элементов массива (массив вводится склавиатуры).
 * + меняет местами максимальный и минимальный элементы массива
 * + выполняет сортировку массива методом пузырька.
 * + выполняет преобразование массива в число.
 **/

import java.util.Scanner;
import java.util.Arrays;

public class task01 {
    public static void main(String[] args) {

        // Вводим массив с клавиатуры
        Scanner scanner = new Scanner(System.in);
        int arraySize = scanner.nextInt();
        int[] myArray = new int[arraySize];
        int currentArrayIndex = 0;
        while (currentArrayIndex < arraySize) {
            myArray[currentArrayIndex] = scanner.nextInt();
            currentArrayIndex++;
        }

        System.out.println("Array Sum = " + arraySum(myArray)); // выводит сумму элементов массива

        reverseArray(myArray); // выполняет разворот массива
        System.out.println("Reversed array : " + Arrays.toString(myArray)); // выводит развернутый массив

        // выводит среднее арифметическое элементов массива
        System.out.printf("Array Average = " + "%.2f\n", arrayAverage(myArray));

        replaceMaxMinInArray(myArray); // процедура меняет местами максимальный и минимальный элементы массива
        // выводит массив после перемещения максимального и минималотного элементов
        System.out.println("Array with replaced Max and Min : " + Arrays.toString(myArray));

        bubbleSortArray(myArray); // процедура выполняет сортировку массива методом пузырька
        // выводит массив после сортировки методом пузырька
        System.out.println("Array arter bubble sorting : " + Arrays.toString(myArray));
        // выводит число преобразованное из массива
        System.out.println("Integer conwerted from array : " + arrayToInteger(myArray));

    }

    public static int arraySum(int[] array) {
        // функция возвращает сумму элементов масива
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum;
    }

    public static void reverseArray(int[] array) {
        // процедура разворачивает массив
        for (int i = 0; i < (array.length) / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
    }

    public static float arrayAverage(int[] array) {
        // функция возвращает среднее арифметическое элементов сассива
        float sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        return sum / array.length;
    }

    public static void replaceMaxMinInArray(int[] array) {
        // процедура меняет местами максимальный и минимальный элементы массива
        if (array.length > 0) {
            int maxValue = array[0];
            int maxIndex = 0;
            int minValue = array[0];
            int minIndex = 0;

            for (int i = 1; i < array.length; i++) {
                if (array[i] > maxValue) {
                    maxValue = array[i];
                    maxIndex = i;
                } else if (array[i] < minValue) {
                    minValue = array[i];
                    minIndex = i;
                }
            }
            array[minIndex] = maxValue;
            array[maxIndex] = minValue;
        }
    }

    public static void bubbleSortArray(int[] array) {
        // процедура выполняет сортировку массива методом пузырька
        for (int leftIndex = 0; leftIndex < array.length; leftIndex++) {
            for (int rightIndex = array.length - 1; rightIndex > leftIndex; rightIndex--) {
                if (array[rightIndex] < array[rightIndex - 1]) {
                    array[rightIndex] = array[rightIndex] + array[rightIndex - 1];
                    array[rightIndex - 1] = array[rightIndex] - array[rightIndex - 1];
                    array[rightIndex] = array[rightIndex] - array[rightIndex - 1];
                }
            }
        }
    }

    public static int arrayToInteger(int[] array) {
        // функция выводит число преобразованное из массива
        int number = 0;
        for (int i : array) {
            number = (number * 10) + i;
        }
        return number;
    }
}
