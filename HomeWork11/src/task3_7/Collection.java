package task3_7;

public interface Collection extends Iterable{
    void add(int element);
    boolean contains(int element);
    int size();
    void removeFirst(int element);
    void print();
}
