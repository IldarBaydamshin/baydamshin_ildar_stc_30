package task3_7;

public interface Iterator {
    int next();
    boolean hasNext();
}
