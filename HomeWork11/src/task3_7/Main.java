package task3_7;

/*
No п/п 3.7
    1. Реализовать следующие методы в ArrayList
     + a. insert
     + b. removeByIndex
    2. Для LinkedList реализовать методы
     + a. reverse() - разворот связного списка
     + b. removeFirst()
     + c. contains()
     + d. insert()
     + e. removeByIndex()
    3. Для LinkedList реализовать паттерн Iterator.
*/

public class Main {
    public static void main(String[] args) {

        List arrayList = new ArrayList();
        for (int i = 0; i <= 20; ++i)  {
            arrayList.add(i);
        }

        System.out.println("=============== ArrayList ================");
        arrayList.print();
        arrayList.insert(10, 9);
        arrayList.print();
        arrayList.removeByIndex(9);
        arrayList.print();



        System.out.println();
        System.out.println("=============== LinkedList ================");
        System.out.println();

        List linkedList = new LinkedList();
        for (int i = 0; i <= 20; ++i)  {
            linkedList.add(i);
        }
        linkedList.print();
        System.out.println("linkedList.contains(0) = " + linkedList.contains(0));
        linkedList.removeFirst(10);
        linkedList.print();
        linkedList.reverse();
        linkedList.print();
        linkedList.insert(999, 10);
        linkedList.print();
        linkedList.removeByIndex(10);
        linkedList.print();

        Iterator linkedListIterator = linkedList.iterator();
        while (linkedListIterator.hasNext()) {
            System.out.print(linkedListIterator.next() + "\t");
        }
    }
}
