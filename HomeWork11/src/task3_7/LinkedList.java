
package task3_7;
public class LinkedList implements List {
    private Node firstNode;
    private Node lastNode;
    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    private class LinkedListIterator implements Iterator {



        private Node current = firstNode ;
        int currentCount = 0;

        @Override
        public int next() {
            int value = current.value;
            current = current.next;
            currentCount++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return currentCount < count;
        }

    }


    @Override
    public int get(int index) {
        if (index < this.count) {
            Node current = firstNode;
            for (int j = 0; j < index; j++) {
                current = current.next;
            }
            System.out.println(current.value);
            return current.value;
        } else {
            System.err.println("Эллемента с индексом " + index + " в этой коллекции нет");
            return -1;
        }
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.firstNode;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        if (index < 0 || index > count) {
            System.out.println("Индекс за пределами коллекции");
        } else {
            if (index == 0) {
                this.firstNode = firstNode.next;
            } else {
                Node nodeToChangeLink = this.firstNode;
                for (int i = 2; i <= index; i++) {
                    nodeToChangeLink = nodeToChangeLink.next;
                }
                nodeToChangeLink.next = nodeToChangeLink.next.next;
            }
            count--;
        }
    }

    @Override
    public void insert(int element, int index) {
        if (index < 0 || index > count) {
            System.out.println("Индекс за пределами коллекции");
        } else {
            Node newNode = new Node(element);
            if (index == 0) {
                newNode.next = this.firstNode;
                firstNode = newNode;
            } else if (index == count) {
                lastNode.next = newNode;
                lastNode = newNode;
            } else {

                Node before = this.firstNode;
                Node current = this.firstNode.next;

                for (int i = 2; i <= index; i++) {
                    before = before.next;
                    current = current.next;
                }
                before.next = newNode;
                newNode.next = current;
                count++;
            }
        }
    }

    @Override
    public void reverse() {
        Node newFirstNode = new Node(firstNode.value);
        Node nextToNewFirst;
        this.lastNode = newFirstNode;

        while (firstNode.next != null) {
            nextToNewFirst = newFirstNode;
            firstNode = firstNode.next;
            newFirstNode = new Node(firstNode.value);
            newFirstNode.next = nextToNewFirst;
        }
        this.firstNode = newFirstNode;
    }


    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (firstNode == null) {
            this.firstNode = newNode;
            this.lastNode = newNode;
        } else {
            lastNode.next = newNode;
            lastNode = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(int element) {
        Node currentNode = firstNode;
        while (currentNode.value != element && currentNode.next != null) {
            currentNode = currentNode.next;
        }
        return currentNode.value == element;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        if (this.contains(element)) {
            if (this.firstNode.value == element) {
                this.firstNode = this.firstNode.next;
            } else {
                Node previousNode = firstNode;
                Node currentNode = firstNode.next;
                while (currentNode.value != element) {
                    previousNode = previousNode.next;
                    currentNode = currentNode.next;
                }
                if (currentNode.next != null) {
                    previousNode.next = currentNode.next;
                } else {
                    previousNode.next = null;
                }
            }
            count--;
        }

    }

    @Override
    public void print() {
        Node current = this.firstNode;
        while (current.next != null) {
            System.out.print(current.value + "\t");
            current = current.next;
        }

        System.out.print(current.value + "\t");
        System.out.println();
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
