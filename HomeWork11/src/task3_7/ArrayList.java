package task3_7;

public class ArrayList implements List {

    private static final int DEFAULT_ARRAY_SIZE = 10;
    private int[] data;
    int count = 0;

    public ArrayList() {
        this.data = new int[DEFAULT_ARRAY_SIZE];
    }

    private class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            ++current;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public int get(int index) {
        if (index < count) {
            return this.data[index];
        }
        return -1;
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (this.data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        for (int i = index; i < count; i++) {
            this.data[i] = this.data[i + 1];
        }
        this.data[count] = 0;
        count--;
    }

    @Override
    public void insert(int element, int index) {
        if (index > count) {
            System.out.println("Максимальный индекс для вставки = " + count + ", индекс со значением = " + index + " вставить нельзя");
        } else {
            if (count == this.data.length - 1) {
                resize();
            }
            count++;
            for (int i = count - 1; i > index; i--) {
                this.data[i] = this.data[i - 1];
            }
            this.data[index] = element;
        }
    }

    @Override
    public void reverse() {

    }

    @Override
    public void print() {
        for (int i = 0; i < count; i++) {
            System.out.print(this.data[i] + "\t");
        }
        System.out.println();
    }

    @Override
    public void add(int element) {
        if (count == this.data.length - 1) {
            resize();
        }
        this.data[count] = element;
        count++;
    }

    private void resize() {
        int newSize = this.data.length + (this.data.length >> 1);
        int[] newData = new int[newSize];

        System.arraycopy(this.data, 0, newData, 0, data.length);
        this.data = newData;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(int element) {
        if (indexOf(element) != -1) {
            int indexOfRemovingElement = indexOf(element);
            for (int i = indexOfRemovingElement; i < count - 1; i++) {
                this.data[i] = this.data[i + 1];
            }
            count--;
        } else {
            System.err.println("Попытка удаления элемента которого в списке нет");
        }
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }

}
