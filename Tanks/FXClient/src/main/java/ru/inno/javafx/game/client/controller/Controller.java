package ru.inno.javafx.game.client.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import ru.inno.javafx.game.client.sockets.SocketClient;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 25.12.2020
 * 43. JavaFXClient
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Controller implements Initializable {

    @FXML
    private Button connectionButton;

    @FXML
    private TextField playerNameTextField;

    @FXML
    private Button startGameButton;

    @FXML
    private TextArea serverLogsTextArea;

    @FXML
    private Circle player;

    @FXML
    private Circle enemy;

    @FXML
    private Rectangle wall;

    @FXML
    private AnchorPane pane;


    @FXML
    private Label timer;

    private SocketClient socketClient;

    private String playerNumber;

//    private int numberOfBullets = 10;


    // событие, которое происходит при нажатии на клавиатуре какой-либо кнопки
    public EventHandler<KeyEvent> keyEventEventHandler = event -> {

        if (event.getCode() == KeyCode.LEFT) {
            socketClient.sendMessage("move LEFT " + playerNumber);
            player.setCenterX(player.getCenterX() - 5);
        } else if (event.getCode() == KeyCode.RIGHT) {
            socketClient.sendMessage("move RIGHT " + playerNumber);
            player.setCenterX(player.getCenterX() + 5);
        } else if (event.getCode() == KeyCode.CONTROL) {
            Circle bullet = new Circle();
            bullet.setRadius(5);
            bullet.setCenterX(player.getCenterX() + player.getLayoutX());
            bullet.setCenterY(player.getCenterY() + player.getLayoutY());
            bullet.setFill(Color.AQUA);
            pane.getChildren().add(bullet);

            AtomicBoolean isHit = new AtomicBoolean(false);
            AtomicBoolean isMiss = new AtomicBoolean(false);
            AtomicInteger step = new AtomicInteger(1);

            Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.005), animation -> {
                bullet.setCenterY(bullet.getCenterY() - step.get());

                if (bullet.getBoundsInParent().intersects(enemy.getBoundsInParent())) {
                    step.set(0);
                    bullet.setVisible(false);
                    pane.getChildren().remove(bullet);
                    if (!isHit.get()) {
                        isHit.set(true);
                        System.out.println("isHit.get() = " + isHit.get());
                        socketClient.sendMessage("hit " + playerNumber + " " + isHit.get());
                    }

                }

                if (bullet.getBoundsInParent().intersects(wall.getBoundsInParent())) {
                    step.set(0);
                    bullet.setVisible(false);
                    pane.getChildren().remove(bullet);

                    if (!isMiss.get()) {
                        isMiss.set(true);
                        System.out.println("isHit.get() = " + isHit.get());
                        socketClient.sendMessage("hit " + playerNumber + " " + isHit.get());
                    }
                }
            }));
            timeline.setCycleCount(500);
            timeline.play();

            socketClient.sendMessage("shot " + playerNumber);
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        serverLogsTextArea.setEditable(false);
        startGameButton.setDisable(true);
        playerNameTextField.setDisable(true);
        connectionButton.setOnAction(event -> {
            socketClient = new SocketClient("localhost", 5000, this);
            connectionButton.setDisable(true);
            startGameButton.setDisable(false);
            playerNameTextField.setDisable(false);
        });

        startGameButton.setOnAction(event -> {
            socketClient.sendMessage("start " + playerNameTextField.getText());
            startGameButton.setDisable(true);
            playerNameTextField.setDisable(true);
            // перемещал фокус на форму
            startGameButton.getScene().getRoot().requestFocus();
        });
    }

    public TextArea getServerLogsTextArea() {
        return serverLogsTextArea;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }

    public Circle getPlayer() {
        return player;
    }

    public Circle getEnemy() {
        return enemy;
    }

    public Rectangle getWall() {
        return wall;
    }

    public String getPlayerNumber() {
        return playerNumber;
    }

    public AnchorPane getPane() {
        return pane;
    }

    public Label getTimer() {
        return timer;
    }


}
