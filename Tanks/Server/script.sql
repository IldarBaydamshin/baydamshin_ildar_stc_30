create table player
(
    player_id           serial  not null
        constraint account_pkey
            primary key,
    player_name         varchar not null,
    last_ip_addr        varchar,
    number_of_games     integer default 0,
    number_of_points    integer default 0,
    number_of_wins      integer default 0,
    number_of_vendettas integer default 0
);

alter table player
    owner to "user";

create table game
(
    game_id         serial not null
        constraint game_pkey
            primary key,
    start_time      timestamp,
    duration        bigint,
    player_a_id     integer
        constraint game_player_a_id_fkey
            references player,
    player_b_id     integer
        constraint game_player_b_id_fkey
            references player,
    number_of_shots integer
);

alter table game
    owner to "user";

create table shot
(
    game_id    integer
        constraint shot_game_id_fkey
            references game,
    shot_id    serial not null,
    shoot_time timestamp,
    shooter    integer,
    target     integer,
    is_hit     boolean
);

alter table shot
    owner to "user";


