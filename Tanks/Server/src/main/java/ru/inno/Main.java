package ru.inno;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.repositories.*;
import ru.inno.server.GameServer;
import ru.inno.service.TanksServiceImpl;


public class Main {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/stc_db_2020");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("user");
        hikariConfig.setPassword("123");
        hikariConfig.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);


        GameServer gameServer
                = new GameServer(new TanksServiceImpl(playersRepository, gamesRepository, shotsRepository));
        gameServer.start(5000);

    }
}
