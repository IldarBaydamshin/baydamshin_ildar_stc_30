package ru.inno.service;

public interface TanksService {
    // срабатывает при входе пользователя в игру
    // если пользователь с таким никнеймом уже есть, то мы используем его
    // если пользователя с таким никнеймом еще нет - то создаем его

//    Player createUpdatePlayerInDB(Player player);

    // срабатываем при начале
//    Game startGame(Game game);

    // срабатывает при завершении игры

//    void finishGameForPlayers(Game game);

    int[] startGame(String firstPlayerNickname, String firstPlayerIpAddress,
                    String secondPlayerNickname, String secondPlayerIpAddress);


    void shot(int gameId, int shooterId, int targetId, boolean hit);
}
