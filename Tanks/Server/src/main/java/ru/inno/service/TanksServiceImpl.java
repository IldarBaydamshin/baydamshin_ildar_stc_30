package ru.inno.service;

import org.postgresql.util.PGTimestamp;
import ru.inno.models.Game;
import ru.inno.models.Player;
import ru.inno.models.Shot;
import ru.inno.repositories.*;

public class TanksServiceImpl implements TanksService {

    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;


    public TanksServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }


    private void createUpdateIp(Player player) {
        if (playersRepository.playerId(player.getPlayerName()) != -1) {
            playersRepository.updateIp(player);
        } else {
            playersRepository.save(player);
        }
        playersRepository.getPlayerData(player);
    }


    @Override
    public int[] startGame(String firstPlayerNickname, String firstPlayerIpAddress,
                           String secondPlayerNickname, String secondPlayerIpAddress) {
        Player firstPlayer = Player.builder()
                .playerName(firstPlayerNickname)
                .lastIpAddress(firstPlayerIpAddress)
                .build();
        Player secondPlayer = Player.builder()
                .playerName(secondPlayerNickname)
                .lastIpAddress(secondPlayerIpAddress)
                .build();

        createUpdateIp(firstPlayer);
        createUpdateIp(secondPlayer);

        Game game = Game.builder()
                .startTime(new PGTimestamp(System.currentTimeMillis()))
                .playerA(firstPlayer.getPlayerId())
                .playerB(secondPlayer.getPlayerId())
                .build();


        return new int[]{gamesRepository.save(game), game.getPlayerA(), game.getPlayerB()};
    }

    @Override
    public void shot(int gameId, int shooterId, int targetId, boolean isHit) {
        Shot shot = Shot.builder()
                .gameId(gameId)
                .shotTime(new PGTimestamp(System.currentTimeMillis()))
                .shooter(shooterId)
                .target(targetId)
                .isHit(isHit)
                .build();

        shotsRepository.save(shot);
    }

}
