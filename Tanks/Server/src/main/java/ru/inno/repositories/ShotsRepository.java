package ru.inno.repositories;

import ru.inno.models.Shot;

public interface ShotsRepository{
    void save(Shot entity);
}
