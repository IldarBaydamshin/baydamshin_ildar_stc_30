package ru.inno.repositories;

import ru.inno.models.Player;

import javax.sql.DataSource;
import java.sql.*;


public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private static String SQL_GET_ID_BY_NAME =
            "select player_id from player where player_name = ?";

    //language=SQL
    private static String SQL_FIND_BY_NAME =
            "select  " +
                    "player_id," +
                    "player_name, " +
                    "last_ip_addr, " +
                    "number_of_games, " +
                    "number_of_points, " +
                    "number_of_wins," +
                    "number_of_vendettas " +
                    "from player " +
                    "where player_name = ?";

    //language=SQL
    private static final String SQL_INSERT =
            "insert into player (" +
                    "player_name, " +
                    "last_ip_addr)" +
                    "values  (?, ?)";

    //language=SQL
    private static final String SQL_UPDATE_IP
            = "UPDATE player SET last_ip_addr = ? WHERE player_id = ?";

    ;


    //language=SQL
    private static final String SQL_UPDATE_ALL_PLAYER =
            "update player set (" +
                    "player_name, " +
                    "last_ip_addr, " +
                    "number_of_games, " +
                    "number_of_points, " +
                    "number_of_wins, " +
                    "number_of_vendettas)" +
                    " = (?, ?, ?, ?, ?, ?) " +
                    "where player_id = ?";


    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Player> playerRowMapper = row -> Player.builder()
            .playerId(row.getInt("player_id"))
            .playerName(row.getString("player_name"))
            .lastIpAddress(row.getString("last_ip_addr"))
            .numberOfGames(row.getInt("number_of_games"))
            .numberOfPoints(row.getInt("number_of_points"))
            .numberOfWins(row.getInt("number_of_wins"))
            .numberOfVendettas(row.getInt("number_of_vendettas"))
            .build();


    @Override
    public boolean save(Player entity) {
        if (playerId(entity.getPlayerName()) == -1) {
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement statement = connection.prepareStatement(SQL_INSERT)) {

                statement.setString(1, entity.getPlayerName());
                statement.setString(2, entity.getLastIpAddress());


                int resultSet = statement.executeUpdate();

                if (resultSet == 0) {
                    System.out.println("Пользователя сохранить не удалось");
                }
                getPlayerData(entity);
                System.out.println("Новый игрок занесен в базу ");
                return resultSet != 0;

            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
        return false;
    }

    @Override
    public boolean update(Player entity) {
        if (playerId(entity.getPlayerName()) != -1) {
            try (Connection connection = dataSource.getConnection();
                 PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_ALL_PLAYER)) {
                statement.setString(1, entity.getPlayerName());
                statement.setString(2, entity.getLastIpAddress());
                statement.setInt(3, entity.getNumberOfGames());
                statement.setInt(4, entity.getNumberOfPoints());
                statement.setInt(5, entity.getNumberOfWins());
                statement.setInt(6, entity.getNumberOfVendettas());
                statement.setInt(7, entity.getPlayerId());

                int updatedRowsCount = statement.executeUpdate();

                if (updatedRowsCount == 0) {
                    System.out.println("Произошла ошибка при обновлении пользоватедля " + entity.getPlayerName());
                }
                return true;
            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }
        }
        return false;
    }

    @Override
    public boolean getPlayerData(Player entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME)) {

            statement.setString(1, entity.getPlayerName());
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                entity.setPlayerId(resultSet.getInt("player_id"));
                entity.setPlayerName(resultSet.getString("player_name"));
                entity.setLastIpAddress(resultSet.getString("last_ip_addr"));
                entity.setNumberOfGames(resultSet.getInt("number_of_games"));
                entity.setNumberOfPoints(resultSet.getInt("number_of_wins"));
                entity.setNumberOfVendettas(resultSet.getInt("number_of_vendettas"));
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public boolean updateIp(Player entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_IP)) {
            statement.setString(1, entity.getLastIpAddress());
            statement.setInt(2, playerId(entity.getPlayerName()));

            int updatedRowsCount = statement.executeUpdate();

            if (updatedRowsCount == 0) {
                System.out.println("Произошла ошибка при обновлении пользоватедля " + entity.getPlayerName());
            }
            System.out.println("Обновлен ip адресс для игрока " + entity.getPlayerName());
            return updatedRowsCount != 0;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public int playerId(String nickMame) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NAME)) {

            statement.setString(1, nickMame);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return -1;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
