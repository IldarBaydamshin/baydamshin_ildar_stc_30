package ru.inno.repositories;

import ru.inno.models.Shot;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;


public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into shot (" +
                    "game_id, " +
                    "shoot_time, " +
                    "shooter, " +
                    "target, " +
                    "is_hit" +
                    ")" +
                    "values (?, ?, ?, ?, ?) ";

    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void save(Shot entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement =
                     connection.prepareStatement(SQL_INSERT)) {
            statement.setInt(1, entity.getGameId());
            statement.setTimestamp(2, entity.getShotTime());
            statement.setInt(3, entity.getShooter());
            statement.setInt(4, entity.getTarget());
            statement.setBoolean(5, entity.isHit());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Информация о выстреле в базе не сохранена");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
