package ru.inno.repositories;

import ru.inno.models.Player;

public interface PlayersRepository {
    boolean save(Player entity);

    boolean update(Player entity);

    boolean getPlayerData(Player entity);

    boolean updateIp(Player entity);

    int playerId(String nickMame);

}
