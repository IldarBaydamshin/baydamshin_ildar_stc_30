package ru.inno.repositories;

import ru.inno.models.Game;

public interface GamesRepository {
    int save(Game entity);

    boolean update(Game entity);

    int getGameId(Game entity);


}
