package ru.inno.repositories;

import ru.inno.models.Game;

import javax.sql.DataSource;
import java.sql.*;

public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_FIND_GAME_ID =
            "select game_id from game where " +
                    "start_time = ? and " +
                    "player_a_id = ? and " +
                    "player_b_id = ?";


    //language=SQL
    private static final String SQL_INSERT_NEW_GAME =
            "insert into game (" +
                    "start_time, " +
                    "player_a_id, " +
                    "player_b_id " +
                    ")" +
                    "values (?, ?, ?) ";

    //language=SQL
    private static final String SQL_UPDATE_GAME =
            "update game set (" +
                    "start_time, " +
                    "duration, " +
                    "player_a_id, " +
                    "player_b_id, " +
                    "number_of_shots " +
                    ")" +
                    "= (?, ?, ?, ?, ?)  " +
                    "where game_id = ?";

    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public int save(Game entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQL_INSERT_NEW_GAME, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setTimestamp(1, entity.getStartTime());
            preparedStatement.setInt(2, entity.getPlayerA());
            preparedStatement.setInt(3, entity.getPlayerB());

            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (preparedStatement.getGeneratedKeys().next()) {
                entity.setGameId(resultSet.getInt("game_id"));
                return resultSet.getInt("game_id");
            }
            System.out.println("Не удалось создать игру");
            return -1;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public boolean update(Game entity) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement =
                     connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setTimestamp(1, entity.getStartTime());
            // game duration in seconds
            statement.setLong(2,
                    (new Time(System.currentTimeMillis()).getTime() - entity.getStartTime().getTime())
                            / 1000);
            statement.setInt(3, entity.getPlayerA());
            statement.setInt(4, entity.getPlayerB());
            statement.setInt(5, entity.getNumberOfShots());
            statement.setInt(6, entity.getGameId());

            int updatedRowCount = statement.executeUpdate();

            return updatedRowCount != 0;

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public int getGameId(Game entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement =
                     connection.prepareStatement(SQL_FIND_GAME_ID)) {
            preparedStatement.setTimestamp(1, entity.getStartTime());
            preparedStatement.setInt(2, entity.getPlayerA());
            preparedStatement.setInt(3, entity.getPlayerB());

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("game_id");
            }
            return save(entity);

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}


