package ru.inno.server;

import ru.inno.service.TanksService;
import ru.inno.service.TanksServiceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {
    private int gameId;
    private int playerOneId;
    private int playerTwoId;

    private Socket playerOne;
    private Socket playerTwo;

    private PlayerThread playerOneThread;
    private PlayerThread playerTwoThread;

    private TanksService tanksService;

    private boolean isGameStarted = false;

    public GameServer(TanksServiceImpl tanksService) {
        this.tanksService = tanksService;
    }

    public void start(int port) {
        try {
            // starts server
            ServerSocket serverSocket = new ServerSocket(port);

            while (!isGameStarted) {
                if (playerOne == null) {
                    playerOne = serverSocket.accept();
                    System.out.println("Первый игрок подключился к серверу");
                    // thread for one player
                    playerOneThread = new PlayerThread(playerOne);
                    playerOneThread.playerValue = "PLAYER_1";

                    playerOneThread.start();
                } else {
                    playerTwo = serverSocket.accept();
                    System.out.println("Второй игрок подключился к серверу");
                    // поток для второго инрока
                    playerTwoThread = new PlayerThread(playerTwo);
                    playerTwoThread.playerValue = "PLAYER_2";
                    playerTwoThread.start();
                    isGameStarted = true;
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }


    // внутренний класс для потока игрока
    private class PlayerThread extends Thread {

        private Socket player;
        private String nickname;
        private String playerValue;


        private BufferedReader fromClient;
        private PrintWriter toClient;

        public PlayerThread(Socket player) {
            this.player = player;
            try {
                this.fromClient = new BufferedReader(new InputStreamReader(player.getInputStream()));
                this.toClient = new PrintWriter(player.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            while (true) {
                try {
                    String messageFromClient = fromClient.readLine();

                    if (messageFromClient != null) {

                        String[] parsedMessageFromClient = messageFromClient.split(" ");
                        if (parsedMessageFromClient[0].equals("start")) {
                            this.nickname = parsedMessageFromClient[1];
                            // if starts from both players
                            if (playerOneThread.nickname != null && playerTwoThread.nickname != null) {
                                int[] gameAndPlayersIds = tanksService.startGame(
                                        playerOneThread.nickname, playerOne.getRemoteSocketAddress().toString().substring(1),
                                        playerTwoThread.nickname, playerTwo.getRemoteSocketAddress().toString().substring(1)
                                );

                                gameId = gameAndPlayersIds[0];
                                playerOneId = gameAndPlayersIds[1];
                                playerTwoId = gameAndPlayersIds[2];

                                playerOneThread.toClient.println("PLAYER_1");
                                playerTwoThread.toClient.println("PLAYER_2");

                                System.out.print("gameId = " + gameId);
                                System.out.print("; playerOneId = " + playerOneId);
                                System.out.println("; playerTwoId = " + playerTwoId);

                                System.out.print("playerOneThread.nickname = " + playerOneThread.nickname);
                                System.out.println("; playerOne.getRemoteSocketAddress() = "
                                        + playerOne.getRemoteSocketAddress().toString().substring(1));

                                System.out.print("playerTwoThread.nickname = " + playerTwoThread.nickname);
                                System.out.print("; playerTwoId = " + playerTwoId);
                                System.out.println("; playerTwo.getRemoteSocketAddress() = "
                                        + playerTwo.getRemoteSocketAddress().toString().substring(1));

                                playerOneThread.toClient.println("Game Started");
                                playerTwoThread.toClient.println("Game Started");

                            }
                        } else if (parsedMessageFromClient[0].equals("hit")) {

                            if (parsedMessageFromClient[1].equals("PLAYER_1")) {

                                System.out.print("gameId = " + gameId + " ");
                                System.out.print("; playerOneId = " + playerOneId);
                                System.out.print("; playerTwoId = " + playerTwoId);
                                System.out.println("; parsedMessageFromClient[2] = " + parsedMessageFromClient[2]);

                                tanksService.shot(gameId, playerOneId, playerTwoId, Boolean.parseBoolean(parsedMessageFromClient[2]));

                            } else if (parsedMessageFromClient[1].equals("PLAYER_2")) {

                                System.out.print("gameId = " + gameId + " ");
                                System.out.print("; playerTwoId = " + playerTwoId);
                                System.out.print("; playerOneId = " + playerOneId);
                                System.out.println("; parsedMessageFromClient[2] = " + parsedMessageFromClient[2]);

                                tanksService.shot(gameId, playerTwoId, playerOneId, Boolean.parseBoolean(parsedMessageFromClient[2]));
                            }
                        }
                    }

                    assert messageFromClient != null;
                    if (!messageFromClient.startsWith("hit")) {
                        playerOneThread.toClient.println(messageFromClient);
                        playerTwoThread.toClient.println(messageFromClient);
                    }

                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

}
