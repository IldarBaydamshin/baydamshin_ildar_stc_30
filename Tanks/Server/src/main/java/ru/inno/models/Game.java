package ru.inno.models;

import lombok.*;

import java.sql.Time;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@Builder

public class Game {
    private int gameId;
    private Timestamp startTime;
    private long duration;
    private int playerA;
    private int playerB;
    private int numberOfShots;
}
