/*
Задание 2.
Реализовать метод бинарного поиска с помощью рекурсии.
class Program {
    public static int binarySearch(int array[], int element) {
    }
}
 */

import java.util.Arrays;

public class task02 {

    public static void main(String[] args) {
        int[] testArray = {1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19};
        int v = 13;
        System.out.println(binarySearch(testArray, v));

    }

    public static int binarySearch(int[] array, int element) {
        int middle = (array.length / 2);
        if (element < array[0] | element > array[array.length - 1]) {
            return -1; // Not in the array
        }

        if (array[middle] == element) {
            return array[middle];
        } else if (element < array[middle]) {
            return binarySearch(Arrays.copyOfRange(array, 0, middle), element);
        } else if (element > array[middle]) {
            return binarySearch(Arrays.copyOfRange(array, middle, array.length), element);
        }
        return -1; // Not int the array
    }
}