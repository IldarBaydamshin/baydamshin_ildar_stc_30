/* No п/п 2.2
	Задание 1 *.
	Реализовать Фибоначчи с однократным вызовом рекурсии.
		- Без f(n-1) + f(n-2)
*/

public class task01 {
    public static void main(String[] args) {
        System.out.println(f(0));
        System.out.println(f(1));
        System.out.println(f(2));
        System.out.println(f(3));
        System.out.println(f(4));
        System.out.println(f(5));
        System.out.println(f(6));
        System.out.println(f(7));
        System.out.println(f(8));
        System.out.println(f(9));
        System.out.println(f(10));
        
    }


    public static int f(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1 | n == 2) {
            return 1;
        }

        int[] array = {1, 1, 2};
        return f(n, 3, array);
    }


    public static int f(int n, int i, int[] array) {
        if (i == n) {
            return array[2];
        }
        array[0] = array[1];
        array[1] = array[2];
        array[2] = array[0] + array[1];
        i++;
        return f(n, i, array);

    }
}
