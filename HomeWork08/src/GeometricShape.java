public abstract class GeometricShape {
    public double centerX;
    public double centerY;

    public GeometricShape() {
        this.centerX = 0;
        this.centerY = 0;
    }

    public abstract double returnArea();

    public abstract double returnPerimeter();

    public String returnCenterXY() {
        return "(" + this.centerX + ", " + this.centerY + ")";
    }

    public void changeCenterXY(double centerX, double centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }

}
