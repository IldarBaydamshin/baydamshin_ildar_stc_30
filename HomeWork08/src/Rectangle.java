public class Rectangle extends Square implements Scalable, Relocatable {
    public double sideB; // second side for Rectangle

    public Rectangle(double sideA, double sideB) {
        super(sideA);
        this.sideB = sideB;
    }

    @Override
    public double returnArea() {
        return this.sideA * this.sideB;
    }

    @Override
    public double returnPerimeter() {
        return 2 * (this.sideA + this.sideB);
    }

    @Override
    public void changeScale(double scaleValue) {
        this.sideA *= scaleValue;
        this.sideB *= scaleValue;
    }

    @Override
    public String returnCenterXY() {
        return super.returnCenterXY();
    }
}
