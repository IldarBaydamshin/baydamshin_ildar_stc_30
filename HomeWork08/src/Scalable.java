public interface Scalable {
    void changeScale(double scaleValue);
}
