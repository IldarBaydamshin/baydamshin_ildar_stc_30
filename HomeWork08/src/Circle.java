public class Circle extends GeometricShape implements Scalable, Relocatable {
    public static final double pi = Math.PI;
    public double radiusA;

    public Circle(double radiusA) {
        this.radiusA = radiusA;
    }

    @Override
    public double returnArea() {
        return pi * this.radiusA * this.radiusA;
    }

    @Override
    public double returnPerimeter() {
        return 2 * pi * this.radiusA;
    }

    @Override
    public void changeScale(double scaleValue) {
        this.radiusA *= scaleValue;
    }

    @Override
    public String returnCenterXY() {
        return super.returnCenterXY();
    }
}
