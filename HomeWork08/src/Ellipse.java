public class Ellipse extends Circle implements Scalable, Relocatable {
    public double radiusB; //Second radius for ellipse

    public Ellipse(double radiusA, double radiusB) {
        super(radiusA);
        this.radiusB = radiusB;
    }

    @Override
    public double returnArea() {
        return pi * this.radiusA * this.radiusB;
    }


    @Override
    public double returnPerimeter() {
        // https://ru.wikipedia.org/wiki/Эллипс (Взята приближенная формула)
        double a = this.radiusA;
        double b = this.radiusB;
        return 4 * (((pi * a * b) + (Math.pow((a - b), (a - b)))) / (a + b));
    }

    @Override
    public String returnCenterXY() {
        return super.returnCenterXY();
    }

}