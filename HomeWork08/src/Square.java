public class Square extends GeometricShape implements Scalable, Relocatable {
    public double sideA;

    public Square(double sideA) {
        this.sideA = sideA;
    }

    @Override
    public double returnArea() {
        return this.sideA * this.sideA;
    }

    @Override
    public double returnPerimeter() {
        return this.sideA * 4;
    }

    @Override
    public String returnCenterXY() {
        return super.returnCenterXY();
    }

    @Override
    public void changeScale(double scaleValue) {
        this.sideA *= scaleValue;
    }
}