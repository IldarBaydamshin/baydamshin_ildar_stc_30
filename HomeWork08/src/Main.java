/*
    No п/п 3.4
    Необходимо реализовать классы предметной области “Геометрические фигуры”./ Geometric shapes
    1. Геометрическая фигура / Geometric shape
    2. Эллипс / Ellipse
    3. Круг / Circle
    4. Прямоугольник / Rectangle
    5. Квадрат / Square
    Необходимо предусмотреть возможность рассчитать площадь и периметр каждой фигуры.
    Исключить дублирование полей и методов для классов.
    Предусмотреть интерфейсы “Масштабируемый” и “Перемещаемый”.
    Данные интерфейсы подразумевают операции по изменению размеров фигур, а также координат их центра.
 */
public class Main {

    public static void main(String[] args) {

        Square square = new Square(2);
        Rectangle rectangle = new Rectangle(2, 3);
        Circle circle = new Circle(2);
        Ellipse ellipse = new Ellipse(2, 3);

        GeometricShape[] geometricShape = {square, rectangle, circle, ellipse};
        Scalable[] shapesToScale = {square, rectangle, circle, ellipse};
        Relocatable[] shapesToRelocate = {square, rectangle, circle, ellipse};

        double newScale = 10;
        double newCenterX = 5;
        double newCenterY = 5;

        System.out.println("(x, y) \t\tArea \t\tPerimeter");
        System.out.println("+++++++++++++++++++++++++++++++++");
        for (GeometricShape shape : geometricShape) {
            System.out.print(shape.returnCenterXY() + "\t");
            System.out.printf("%.2f\t\t", shape.returnArea());
            System.out.printf("%.2f\n", shape.returnPerimeter());
        }

        for (Scalable shape : shapesToScale) {
            shape.changeScale(newScale);
        }

        for (Relocatable shape : shapesToRelocate) {
            shape.changeCenterXY(newCenterX, newCenterY);
        }

        System.out.println("+++++++++++++++++++++++++++++++");
        for (GeometricShape shape : geometricShape) {
            System.out.print(shape.returnCenterXY() + "\t");
            System.out.printf("%.2f\t\t", shape.returnArea());
            System.out.printf("%.2f\n", shape.returnPerimeter());
        }
    }
}
