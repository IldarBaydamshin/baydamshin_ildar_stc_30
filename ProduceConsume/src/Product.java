public class Product {
    private boolean isReady;

    public boolean isProduces() {
        return isReady;
    }

    public boolean isConsumed() {
        return !isReady;
    }

    public void produce() {
        this.isReady = true;
    }

    public void consume() {
        this.isReady = false;
    }

}
