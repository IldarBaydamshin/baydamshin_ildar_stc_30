/*
 Многопоточность
No п/п 6.1
Необходимо рассчитать сумму элементов случайного массива несколькими потоками.
Пример работы программы (каждый элемент массива равен единице):
Sum: 13
Thread 1: from 0 to 4 sum is 5 Thread 2: from 5 to 9 sum is 5 Thread 3: from 10 to 12 sum is 3 Sum by threads: 13
● Каждый поток считает свой “участок”
● Вывод может быть непоследовательным
 */


public class Main {

    public static void main(String[] args) {
        // write your code here
        Product product = new Product();
        Consumer consumer = new Consumer(product);
        Producer producer = new Producer(product);

        consumer.start();
        producer.start();



    }
}
