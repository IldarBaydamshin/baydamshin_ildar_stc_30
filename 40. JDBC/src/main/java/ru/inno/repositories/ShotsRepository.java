package ru.inno.repositories;


import ru.inno.models.Shot;
import java.util.List;

public interface ShotsRepository extends CrudRepository<Shot, Integer>{
    List<Shot> findAllByGameId(Integer gameId);
}
