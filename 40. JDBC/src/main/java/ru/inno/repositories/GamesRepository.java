package ru.inno.repositories;

import ru.inno.models.Game;

import java.util.List;

public interface GamesRepository extends CrudRepository<Game, Integer> {
    List<Game> findAllByPlayerId(Integer playerId);
}
