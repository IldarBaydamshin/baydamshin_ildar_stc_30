package ru.inno.repositories;

import ru.inno.models.Player;

import java.util.List;

public interface PlayersRepository extends CrudRepository<Player, Integer>{
    List<Player> findAllByPoints(Integer points);
}
