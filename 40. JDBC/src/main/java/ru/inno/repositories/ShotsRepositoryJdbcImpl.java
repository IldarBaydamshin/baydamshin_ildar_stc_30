package ru.inno.repositories;

import ru.inno.models.Shot;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Optional;

public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into shot (" +
                    "game_id, " +
                    "shoot_time, " +
                    "shooter, " +
                    "target, " +
                    "is_hit" +
                    ")" +
                    "values (?, ?, ?, ?, ?) ";

    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void save(Shot entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement =
                     connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setInt (1, entity.getGameId());
            statement.setTimestamp (2, entity.getShotTime());
            statement.setInt(3, entity.getShooter());
            statement.setInt(4, entity.getTarget());
            statement.setBoolean(5, entity.isHit());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }

            ResultSet generatedIds = statement.getGeneratedKeys();

            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("shot_id");
                entity.setGameId(generatedId);
            } else {
                throw new SQLException("Cannot return shot_id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Shot entity) {

    }

    @Override
    public void delete(Integer integer) {

    }

    @Override
    public Optional<Shot> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public List<Shot> findAll() {
        return null;
    }

    @Override
    public List<Shot> findAllByGameId(Integer gameId) {
        return null;
    }
}
