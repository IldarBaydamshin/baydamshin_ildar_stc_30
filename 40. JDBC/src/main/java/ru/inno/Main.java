package ru.inno;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.models.Game;
import ru.inno.models.Player;
import ru.inno.models.Shot;
import ru.inno.repositories.*;

import java.sql.Timestamp;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/stc_db_2020");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("user");
        hikariConfig.setPassword("123");
        hikariConfig.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);

        Player player = Player.builder()
                .playerName("Player-2")
                .lastIpAddress("100.1.1.2")
                .numberOfGames(0)
                .numberOfPoints(0)
                .numberOfWins(0)
                .numberOfVendettas(0)
                .build();

        playersRepository.save(player);
        System.out.println(player.getPlayerId());
        System.out.println(playersRepository.findAllByPoints(3));

        System.out.println(playersRepository.findAll());

        Optional<Player> playerOptional = playersRepository.findById(20);
        if (playerOptional.isPresent()) {
            System.out.println(playerOptional.get());
        } else {
            System.out.println("Нет такого пользователя");
        }


        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        Game game = Game.builder()
                .startTime(new Timestamp(System.currentTimeMillis()))
                .duration(120)
                .playerA(1)
                .playerB(2)
                .numberOfShots(20)
                .build();

        gamesRepository.save(game);
        System.out.println(game.getGameId());

        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        Shot shot= Shot.builder()
                .gameId(1)
                .shotTime(new Timestamp(System.currentTimeMillis()))
                .shooter(1)
                .target(2)
                .isHit(false)
                .build();

        shotsRepository.save(shot);
        System.out.println(shot.getShotId());


    }
}
