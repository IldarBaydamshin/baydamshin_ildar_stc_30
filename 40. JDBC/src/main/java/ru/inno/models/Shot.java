package ru.inno.models;

import lombok.*;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@Builder

public class Shot {
    private int gameId;
    private int shotId;
    private Timestamp shotTime;
    private int shooter;
    private int target;
    private boolean isHit;
}
