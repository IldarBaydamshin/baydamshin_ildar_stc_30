/*
Многопоточность

Необходимо рассчитать сумму элементов случайного массива несколькими потоками.

Пример работы программы (каждый элемент массива равен единице):

Thread 1: from 0 to 4 sum is 5
Thread 2: from 5 to 9 sum is 5
Thread 3: from 10 to 12 sum is 3
Sum by threads: 13

● Каждый поток считает свой “участок”
● Вывод может быть непоследовательным
*/

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();

        int[] array = new int[100000000];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int range = array.length / 3;
        int[] result = new int[3];

        submit(() -> {
            for (int i = 0; i < range; i++) {
                result[0] += array[i];

            };
            System.out.println("Thread 1: from " + 0  + " to " + (range - 1) + " sum is  " + result[0]);
        });

        submit(() -> {
            for (int i = range; i < 2 * range; i++) {
                result[1] += array[i];
            };

            System.out.println("Thread 2: from " + range  + " to " + (2 * range) + " sum is  " + result[1]);
        });

        submit(() -> {
            for (int i = 2* range; i < array.length; i++) {
                result[2] += array[i];
            };

            System.out.println("Thread 3: from " + 2 * range  + " to " + (array.length - 1) + " sum is  " + result[2]);
        });


        System.out.println("Sum by threads: " +  Arrays.stream(result).sum());
    }

    public static void submit(Runnable task) {
        Thread newThread = new Thread(task);
        newThread.start();
    }

}


