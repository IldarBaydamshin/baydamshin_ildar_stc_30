import java.sql.*;

public class Main {

    private static final String PG_USER = "user";
    private static final String PG_PASSWORD = "123";
    private static final String PG_URL = "jdbc:postgresql://localhost:5432/stc_db_2020";

    public static void main(String[] args) {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection(PG_URL, PG_USER, PG_PASSWORD);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select last_ip_addr, account_name from account");

            while (resultSet.next()) {
                System.out.println(resultSet.getString("last_ip_addr")
                        + "  " + resultSet.getString("account_name"));
            }

        } catch (SQLException throwables) {
            throw new IllegalArgumentException(throwables);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        }
    }
}
