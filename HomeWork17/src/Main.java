import models.Course;
import models.Lesson;
import models.Tutor;
import service.EducationCentreService;

public class Main {
    public static void main(String[] args) {
        EducationCentreService service = new EducationCentreService();

        //Работа с объектом курсы
        Course newCourse = new Course("STC-6", "Java EE-2",
                "10-11-2020", "17-01-2021",
                "[Учитель-3; Учитель-5]", "[урок-1; урок-2; урок-3]");

        if (service.findCourse(newCourse.getId()) == null) {
            service.saveCourse(newCourse.asRecordToFile());
            System.out.println(service.findCourse("STC-6").asRecordToFile());
        } else {
            System.out.println(newCourse.getId() + " Already exist");
        }


        // Работа с обектос учетиля
        Tutor newTutor = new Tutor("Андрей", "Антонов",
        "9", "[STC-15; STC-16]");

        if (service.findTutor(newTutor.getFirstName()) == null) {
            service.saveTutor(newTutor.asRecordToFile());
            System.out.println(service.findTutor("Андрей").asRecordToFile());
        } else {
            System.out.println(newTutor.getFirstName() + " Already exist");
        }

        // Работа с обектом уроки
        Lesson newLesson = new Lesson("19:00 25-09-2020", "STC-30",
                "Основы промышленной разработки ПО на Java");
        if (service.findLesson(newLesson.getDaytime()) == null) {
            service.saveLesson(newLesson.asRecordToFile());
            System.out.println(service.findLesson("19:00 25-09-2020").asRecordToFile());
        } else {
            System.out.println(newLesson.getDaytime() + " Already exist");
        }
    }
}
