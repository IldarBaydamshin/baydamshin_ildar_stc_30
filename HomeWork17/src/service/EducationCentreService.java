package service;

import dao.*;
import models.*;

public class EducationCentreService {
    private final Dao courseDao = new CourseDaoImpl();
    private final Dao tutorsDao = new TutorsDaoImpl();
    private final Dao lessonsDao = new LessonsDaoImpl();

    public Course findCourse(String string) {
        if (courseDao.read(string) != null) {
            return new Course(courseDao.read(string));
        }
        return null;
    }

    public Tutor findTutor(String string) {
        if (tutorsDao.read(string) != null) {
            return new Tutor(tutorsDao.read(string));
        }
        return null;
    }

    public Lesson findLesson(String string) {
        if (lessonsDao.read(string) != null) {
            return new Lesson(lessonsDao.read(string));
        }
        return null;
    }

    public void saveCourse(String string) {
        courseDao.add(string);
    }

    public void saveTutor(String string) {
        tutorsDao.add(string);
    }

    public void saveLesson(String string) {
        lessonsDao.add(string);
    }
}