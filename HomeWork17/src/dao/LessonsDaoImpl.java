package dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class LessonsDaoImpl implements Dao {
    private final File file = new File("lessons.txt");

    @Override
    public void add(String string) {
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write(string + "\n");
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String[] read(String string) {
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                if (line.split(",")[0].contentEquals(string)) {
                    scanner.close();
                    return line.split(",");
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
