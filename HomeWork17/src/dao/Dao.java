package dao;

public interface Dao {
    void add(String string);
    String[] read(String string);
}
