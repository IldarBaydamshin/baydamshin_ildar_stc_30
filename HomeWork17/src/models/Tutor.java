/*
● Модель “Преподаватель” содержит:
    ○ Имя
    ○ Фамилия
    ○ Стаж работы
    ○ Список курсов
 */

package models;

public class Tutor {
    private String firstName;
    private String secondName;
    private String recordOfService;
    private String courses;

    public String asRecordToFile() {
        return firstName + ", " +
                secondName + ", " +
                recordOfService + ", " +
                courses;
    }

    public Tutor(String firstName, String secondName, String recordOfService, String courses) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.recordOfService = recordOfService;
        this.courses = courses;
    }

    public Tutor(String[] args) {
        this.firstName = args[0];
        this.secondName = args[1];
        this.recordOfService = args[2];
        this.courses = args[3];
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getRecordOfService() {
        return recordOfService;
    }

    public void setRecordOfService(String recordOfService) {
        this.recordOfService = recordOfService;
    }

    public String getCourses() {
        return courses;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }
}
