/*
● Модель “Урок” содержит:
    ○ Время/дата проведения урока
    ○ Название
    ○ Курс
 */

package models;

public class Lesson {
    private String daytime;
    private String name;
    private String course;

    public Lesson(String daytime, String name, String course) {
        this.daytime = daytime;
        this.name = name;
        this.course = course;
    }

    public Lesson(String[] arg) {
        this.daytime = arg[0];
        this.name = arg[1];
        this.course = arg[2];
    }

    public String asRecordToFile() {
        return daytime + ", " +
                name + ", " +
                course;
    }

    public String getDaytime() {
        return daytime;
    }

    public void setDaytime(String daytime) {
        this.daytime = daytime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
