/*
● Модель “Курс” содержит:
    ○ Идентификатор курса
    ○ Название курса
    ○ Дата начала
    ○ Дата окончания
    ○ Список преподавателей
    ○ Список уроков курса
 */

package models;

public class Course {
    private String id;
    private String name;
    private String starDate;
    private String endDate;
    private String tutors;
    private String lessons;

    public Course(String id, String name, String starDate, String endDate, String tutors, String lessons) {
        this.id = id;
        this.name = name;
        this.starDate = starDate;
        this.endDate = endDate;
        this.tutors = tutors;
        this.lessons = lessons;
    }

    public Course(String[] args) {
        this.id = args[0];
        this.name = args[1];
        this.starDate = args[2];
        this.endDate = args[3];
        this.tutors = args[4];
        this.lessons = args[5];
    }

    public String asRecordToFile() {
        return id + ", " +
                name + ", " +
                starDate + ", " +
                endDate + ", " +
                tutors + ", " +
                lessons;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStarDate() {
        return starDate;
    }

    public void setStarDate(String starDate) {
        this.starDate = starDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getTutors() {
        return tutors;
    }

    public void setTutors(String tutors) {
        this.tutors = tutors;
    }

    public String getLessons() {
        return lessons;
    }

    public void setLessons(String lessons) {
        this.lessons = lessons;
    }
}


