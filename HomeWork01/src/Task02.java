/*
Реализовать приложение, которое выводит двоичное представление пятизначного числа
(значение числа задается непосредственно в коде).
*/

public class Task02 {
    public static void main(String[] args) {
        int number = 12345;

        // *** Line number 6 can replace lines 8 - 42 ))) ***
        // String binaryNumber = Integer.toBinaryString(number);

        String binaryNumber = "";

        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;
        number /= 2;
        binaryNumber = Integer.toString(number % 2) + binaryNumber;

        System.out.println(binaryNumber);
    }
}
