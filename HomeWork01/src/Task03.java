/*
Задание 3.
Реализовать приложение, которое для заданной последовательности чисел считает произведение тех чисел,
сумма цифр которых - простое число. Последнее число последовательности - 0.
Пример входных данных:
119 (сумма цифр - 11, простое число)
33542 (сумма цифр - 17, простое число)
99 (сумма цифр - 18, составное число)
991 (сумма цифр - 20, составное число)
Ответ: 119 * 33542 = 3991498
*/

import java.util.Scanner;

public class Task03{
	public static void main(String[] args) {

		int result = 0;

		Scanner scanner = new Scanner (System.in);
		int number = scanner.nextInt();

		boolean numberNotZero = number > 0;
		while (numberNotZero) {

			int digitsSum = 0;
			int currentNumber = number;
			boolean countNotReady = number > 0;
			while (countNotReady) {
				digitsSum += number % 10;
				number /= 10;
				countNotReady = number > 0;
			}

			boolean isSimple = true;
			int currentDevider = 2;
			int maxDevider = digitsSum / 2;
			while (isSimple & currentDevider <= maxDevider){
				if (digitsSum == digitsSum / currentDevider * currentDevider){
					isSimple = false;
				}
				currentDevider ++;
			}

			if (isSimple){
				if (result == 0){
					result += currentNumber;
				}else {
					result *= currentNumber;
				}
			}

			number = scanner.nextInt();
			numberNotZero = number > 0;
		}
		System.out.println(result);
	}
}
