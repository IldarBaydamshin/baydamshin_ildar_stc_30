/*
Реализовать приложение, которое выводит сумму цифр пятизначного числа
(значение числа задается непосредственно в коде).
Sum of digits of five-place number
*/

class Task01 {
    public static void main(String[] args) {
        int number = 12345;
        int digitsSum = number / 10000;
        digitsSum += number % 10000 / 1000;
        digitsSum += number % 1000 / 100;
        digitsSum += number % 100 / 10;
        digitsSum += number % 10;
        System.out.println(digitsSum);
    }
}
