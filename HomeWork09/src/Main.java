/*
 No п/п 3.5
 Необходимо реализовать следующий набор интерфейсов и их лямбда-реализаций:
    NumbersProcess -
        + метод int process(int number)
          реализации:
            - развернуть исходное число
            - убрать нули из исходного числа
            - заменить нечетные цифры ближайшей четной снизу (например, 7 на 6)

   StringsProcess -
        + метод String process(String string)
          реализации:
            - развернуть исходную строку
            - убрать все цифры из строки
            - сделать все маленькие буквы БОЛЬШИМИ

 Также необходимо реализовать класс NumbersAndStringProcessor.
 Данный класс принимает на вход массив строк и массив чисел и выполняет преобразования массивов:
        + String[] process(StringProcess process)
        + int[] process(NumbersProcess process)

 В main продемонстрировать работу классов и интерф ейсов.
*/

public class Main {

    public static void main(String[] args) {
        // write your code here
        int[] sourceIntArray = {61716, 1, 100000003, 102000003, 102034005, 102304506, 102340567, 123405678};
        String[] sourceStringArray = {"ze0ro", "o1ne", "t2wo", "th3ree", "fo4ur", "fi5ve", "si6x", "se7ven", "eig8ht", "ni9ne"};
        NumbersAndStringProcessor numberAndStringArrays = new NumbersAndStringProcessor(sourceIntArray, sourceStringArray);

        numberAndStringArrays.printNumbersArray();

        // разворот исходного числа
        numberAndStringArrays.mapNumber(number -> {
            int newNumber = 0;
            while (number != 0) {
                newNumber = newNumber * 10 + number % 10;
                number /= 10;
            }
            return newNumber;
        });
        numberAndStringArrays.printNumbersArray();

        // удаление нулей из ис[одного числа
        numberAndStringArrays.mapNumber(number -> {
            int newNumber = 0;
            int multiplier = 1;

            while (number != 0) {
                newNumber += number % 10 * multiplier;
                if (number % 10 != 0) {
                    multiplier *= 10;
                }
                number /= 10;
            }
            return newNumber;
        });
        numberAndStringArrays.printNumbersArray();

        // заменяет значения нечетных цифр на ближайшую четную снизу (например, 7 на 6)
        numberAndStringArrays.mapNumber(number -> {
            int newNumber = 0;
            int multiplier = 1;
            while (number != 0) {
                if ((number % 10) % 2 == 0) {
                    newNumber += number % 10 * multiplier;
                } else {
                    newNumber += ((number % 10) - 1) * multiplier;
                }
                multiplier *= 10;
                number /= 10;
            }
            return newNumber;
        });
        numberAndStringArrays.printNumbersArray();

        System.out.println();
        numberAndStringArrays.printStringArray();

        // разворот исходной строки
        numberAndStringArrays.mapString(string -> {
            char temp;
            char[] stringAsCharArray = string.toCharArray();
            for (int left = 0, right = string.length() - 1; left < right; left++, right--) {
                temp = stringAsCharArray[left];
                stringAsCharArray[left] = stringAsCharArray[right];
                stringAsCharArray[right] = temp;
            }
            return String.valueOf(stringAsCharArray);
        });
        numberAndStringArrays.printStringArray();

        // убираем цифры из строк
        numberAndStringArrays.mapString(string -> {
            char[] stringAsCharArray = string.toCharArray();
            String[] stringAsArray = string.split("");
            for (int i = 0; i < string.length(); i++) {
                if (Character.isDigit(stringAsCharArray[i])) {
                    stringAsArray[i] = "";
                }
                string = String.join("", stringAsArray);
            }
            return string;
        });
        numberAndStringArrays.printStringArray();

        //делает все маленькие буквы БОЛЬШИМИ
        numberAndStringArrays.mapString(string -> {
            char[] stringAsCharArray = string.toCharArray();
            String[] stringAsArray = string.split("");
            for (int i = 0; i < string.length(); i++) {
                if (Character.isLowerCase(stringAsCharArray[i])) {
                    stringAsArray[i] = String.valueOf(Character.toUpperCase(stringAsCharArray[i]));
                }
                string = String.join("", stringAsArray);
            }
            return string;
        });
        numberAndStringArrays.printStringArray();
    }
}
