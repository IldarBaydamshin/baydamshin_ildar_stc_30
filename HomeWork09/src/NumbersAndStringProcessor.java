import java.util.Arrays;

/*
Данный класс принимает на вход массив строк и массив чисел и выполняет преобразования массивов:
  + String[] process(StringProcess process)
  + int[] process(NumbersProcess process)
 */
public class NumbersAndStringProcessor {
    private int[] intSequence;
    private String[] stringSequence;

    //constructor for string and number arrays
    public NumbersAndStringProcessor(int[] intSequence, String[] stringSequence) {
        this.intSequence = new int[intSequence.length];
        this.stringSequence = new String[stringSequence.length];
        // creating copies of given arrays
        System.arraycopy(stringSequence, 0, this.stringSequence, 0, stringSequence.length);
        System.arraycopy(intSequence, 0, this.intSequence, 0, intSequence.length);
    }

    // mapping elements of sting array using method that will be defined via StringProcess interface
    public NumbersAndStringProcessor mapString(StringsProcess stringsProcess) {
        for (int i = 0; i < stringSequence.length; i++) {
            this.stringSequence[i] = stringsProcess.process(stringSequence[i]);
        }
        return this;
    }

    // mapping elements of numbers array that will be defined via NumberProcess interface
    public NumbersAndStringProcessor mapNumber(NumbersProcess numbersProcess) {
        for (int i = 0; i < intSequence.length; i++) {
            this.intSequence[i] = numbersProcess.process(intSequence[i]);
        }
        return this;
    }

    public String[] getStringSequence() {
        return stringSequence;
    }

    public int[] getIntSequence() {
        return intSequence;
    }

    public void printStringArray() {
        System.out.println(Arrays.toString(this.stringSequence));
    }

    public void printNumbersArray() {
        System.out.println(Arrays.toString(this.intSequence));
    }
}
