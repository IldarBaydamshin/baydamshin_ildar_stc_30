public class TV {
    private String tvName;
    private int channelNumber;
    private String channelName;
    private String programName;

    TV(String tvName) {
        this.tvName = tvName;
    }

    public String getTvName() {
        return tvName;
    }

    public void setChannelNumber(int channelNumber) {
        if (channelNumber > 0 && channelNumber < 11) {
            this.channelNumber = channelNumber;
        } else if (channelNumber > 10) {
            this.channelNumber = 0;
            System.out.println("На " + tvName + " канал номер " + channelNumber + " не настроен");
        }
    }

    public int getChannelNumber() {
        return this.channelNumber;
    }

    public String getChannelName() {
        return this.channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public void showProgram() {
        if (channelNumber > 0 && channelNumber < 11) {
            System.out.println("На экране: \n     Канал: " + channelName + "\n Программа: " + programName);
        }
    }
}
