public class Channel {
    private String [] channelsListName;
    private int channelNumber = 0;
    private String currentChannelName;
    private String currentProgramName;


    Channel(int numberOfChannels) {
        this.channelsListName = new String[numberOfChannels + 1];
    }

    public String getChannelsListName(int channelNumber) {
        return this.channelsListName[channelNumber];
    }

    public void addChannelToList(int channelNumber, String channelName) {
        this.channelsListName[channelNumber] = channelName;
    }

    public int getNumberOfChannels() {
        return this.channelsListName.length - 1;
    }

    public void setChannel(int channelNumber) {
        this.currentChannelName = channelsListName[channelNumber];
        this.channelNumber = channelNumber;
    }

    public String getChannelName() {
        return this.currentChannelName;
    }

    public int getChannelNumber() {

        return this.channelNumber;
    }

    public void setProgramName (String programName) {
        this.currentProgramName = programName;
    }

    public String getCurrentProgramName() {
        return currentProgramName;
    }
}
