import java.util.Random;

public class Program {
    private String[][] programsList;
    private String currentProgramName;

    Program(int numOfChannels, int numOfProgramsPerChannel) {
        this.programsList = new String[numOfChannels + 1][numOfProgramsPerChannel];
    }

    public int getNumberOfProgramsInChannel(int channelNumber) {
        return this.programsList[channelNumber].length;
    }

    public void addProgram(String programName, int channelNumber, int programNumber) {
        this.programsList[channelNumber][programNumber] = programName;
    }

    public void setRandomProgram(int channelNumber) {
        Random random = new Random();
        this.currentProgramName = this.programsList[channelNumber][random.nextInt(programsList[channelNumber].length)];
    }

    public String getProgramName() {
        return this.currentProgramName;
    }

}