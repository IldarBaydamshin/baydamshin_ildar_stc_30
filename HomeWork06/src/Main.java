/*
    Выполнить моделирование предметной области “Телевизор” (см. UML диграмму)
    1.  В классе RemoteController предусмотреть метод, который позволит по номеру канала “показать”
        СЛУЧАЙНУЮ программу этого канала.
    2.  Инициализацию всех объектов и демонстрацию работоспособности программы продемонстрировать в main-методе.
*/
public class Main {
    public static void main(String[] args) {

        RemoteController remoteController = new RemoteController("Пульте SONY"); // определяем название пульта
        TV tv = new TV("Телевизоре SONY"); //определяем название телевизора
        Channel channels = new Channel(10); // в рамках задачи ограничимся только 10 каналами
        Program programs = new Program(channels.getNumberOfChannels(), 24); // в рамках задачи определим 24 программы (по программе на кажды1 час)
        // Создаем телеканалы каналы

        channels.addChannelToList(1, "Первый канал");
        channels.addChannelToList(2, "Россия-1");
        channels.addChannelToList(3, "МатчТВ");
        channels.addChannelToList(4, "Россия-24");
        channels.addChannelToList(5, "Россия-К");
        channels.addChannelToList(6, "НТВ");
        channels.addChannelToList(7, "Карусель");
        channels.addChannelToList(8, "CTC");
        channels.addChannelToList(9, "THT");
        channels.addChannelToList(10, "BBC");

        // примитивный цикл для заполняем название каналов
        for (int i = 1; i < channels.getNumberOfChannels() + 1; i++) {
            for (int j = 0; j < programs.getNumberOfProgramsInChannel(i); j++) {
                programs.addProgram("Prog-" + j, i, j);
            }
        }

        for (int i = - 1; i <13; i++) {
            remoteController.setChannelNumber(i);
            tv.setChannelNumber(remoteController.getChannelNumber());
            channels.setChannel(tv.getChannelNumber());
            programs.setRandomProgram(channels.getChannelNumber());
            channels.setProgramName(programs.getProgramName());
            tv.setChannelName(channels.getChannelName());
            tv.setProgramName(channels.getCurrentProgramName());
            tv.showProgram();
            System.out.println("_____________");
        }

    }
}
