public class RemoteController {
    private String remoteControllerName;
    private int channelNumber;

    RemoteController(String remoteControllerName) {
        this.remoteControllerName = remoteControllerName;
        this.channelNumber = 0;
    }

    public String getRemoteControllerName() {
        return remoteControllerName;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public void setChannelNumber(int newChannelNumber) {
        if (newChannelNumber > 0) {
            this.channelNumber = newChannelNumber;
            System.out.println("На " + remoteControllerName + " включили канал с номером " + newChannelNumber);
        } else {
            System.out.println("На телевизоре канала с номером " + newChannelNumber + " нет");
        }
    }
}