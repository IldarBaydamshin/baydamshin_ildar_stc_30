package task3_9;

public class ArrayList<T> implements List<T> {

    private static final int DEFAULT_ARRAY_SIZE = 10;
    private T[] data;
    int count = 0;

    public ArrayList() {
        this.data = (T[]) new Object[DEFAULT_ARRAY_SIZE];
    }

    private class ArrayListIterator implements Iterator<T> {

        private int current = 0;

        @Override
        public T next() {
            T value = data[current];
            ++current;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public T get(int index) {
        if (index < count) {
            return this.data[index];
        }
        return null;
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (this.data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        for (int i = index; i < count; i++) {
            this.data[i] = this.data[i + 1];
        }
        this.data[count] = null;
        count--;
    }

    @Override
    public void insert(T element, int index) {
        if (index > count) {
            System.out.println("Максимальный индекс для вставки = " + count + ", индекс со значением = " + index + " вставить нельзя");
        } else {
            if (count == this.data.length - 1) {
                resize();
            }
            count++;
            for (int i = count - 1; i > index; i--) {
                this.data[i] = this.data[i - 1];
            }
            this.data[index] = element;
        }
    }

    @Override
    public void reverse() {
        for (int left = 0, right = count - 1; left < right; left++, right--) {
            data[count] = data[left];
            data[left] = data[right];
            data[right] = data[count];
        }
        data[count] = null;
    }

    @Override
    public void print() {
        for (int i = 0; i < count; i++) {
            System.out.print(this.data[i] + "\t");
        }
        System.out.println();
    }

    @Override
    public void add(T element) {
        if (count == this.data.length - 1) {
            resize();
        }
        this.data[count] = (T) element;
        count++;
    }

    private void resize() {
        int newSize = this.data.length + (this.data.length >> 1);
        System.out.println("this.data.length = " + this.data.length);
        System.out.println("(this.data.length >> 1) = " + (this.data.length >> 1));
        T[] newData = (T[]) new Object[newSize];
        for (int i = 0; i < this.data.length; i++) {
            newData[i] = this.data[i];
        }
        this.data = newData;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(T element) {
        if (indexOf(element) != -1) {
            int indexOfRemovingElement = indexOf(element);
            for (int i = indexOfRemovingElement; i < count - 1; i++) {
                this.data[i] = this.data[i + 1];
            }
            count--;
        } else {
            System.err.println("Попытка удаления элемента которого в списке нет");
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }
}
