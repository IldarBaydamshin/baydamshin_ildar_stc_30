package task3_9;

public interface Iterator<A> {
    A next();
    boolean hasNext();
}
