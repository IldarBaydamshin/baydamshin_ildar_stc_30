package task3_9;

public interface Iterable <B> {
    Iterator<B> iterator();

}
