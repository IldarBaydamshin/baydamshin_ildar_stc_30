package task3_9;

public interface List<D> extends Collection<D>{
    D get(int index);
    int indexOf(D element);
    void removeByIndex(int index);
    void insert(D element, int index);
    void reverse();
}
