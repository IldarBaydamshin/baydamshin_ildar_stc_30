package task3_9;

/*
    No п/п 3.9
    1. Необходимо реализовать обобщенные варианты коллекций LinkedList и ArrayList (включая все вложенные классы).
*/

public class Main {
    public static void main(String[] args) {

        System.out.println("\n" + "=============== LinkedList ================" + "\n");

        List<String> linkedList = new LinkedList<>();
        for (int i = 0; i <= 20; ++i) {
            linkedList.add(i + " ");
        }

        linkedList.print();
        System.out.println("linkedList.contains(0) = " + linkedList.contains("0"));
        linkedList.removeFirst("10");
        linkedList.print();
        linkedList.reverse();
        linkedList.print();
        linkedList.insert("999", 10);
        linkedList.print();
        linkedList.removeByIndex(10);
        linkedList.print();
        System.out.println("linkedList.get(10) = " + linkedList.get(10));
        System.out.println("linkedList.size() = " + linkedList.size());

        Iterator linkedListIterator = linkedList.iterator();
        while (linkedListIterator.hasNext()) {
            System.out.print(linkedListIterator.next() + "\t");
        }

        System.out.println("\n\n" + "=============== ArrayList ================" + "\n");

        List<String> arrayList = new ArrayList<>();
        for (int i = 0; i <= 20; ++i) {
            arrayList.add(i + "S");
        }
        arrayList.print();
        arrayList.insert("10", 9);
        arrayList.print();
        arrayList.removeByIndex(9);
        arrayList.print();
        System.out.println("arrayList.get(9) = " + arrayList.get(9));
        arrayList.reverse();
        arrayList.print();
        System.out.println("arrayList.size() = " + arrayList.size());

    }
}
