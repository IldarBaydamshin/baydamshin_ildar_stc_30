/*
No п/п 3.10
    1. Реализовать в HashMap метод void put(K key, V value)с учетом замены значения value.
        Например:
            map.put(“Marsel”, 26);
            map.put(“Marsel”, 27);
        В таком случае в HashMap должен быть только одно значение 27 под ключом Marsel.
        Также необходимо реализовать метод V get(K key).

    2. Реализовать класс HashSet, базирующийся на использовании HashMap.
 */

package ru.inno;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMapImpl<>();


        map.put("Марсель", 26);
        map.put("Марсель", 27);

        map.put("Денис", 30);
        map.put("Илья", 28);
        map.put("Неля", 18);
        map.put("Катерина", 23);

        map.put("Полина", 15);
        map.put("Полина", 18);

        map.put("Регина", 18);

        map.put("Максим", 20);
        map.put("Максим", 26);

        map.put("Сергей", 18);
        map.put("Иван", 18);
        map.put("Виктор", 18);

        map.put("Виктор Александрович", 31);

        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Денис"));
        System.out.println(map.get("Илья"));
        System.out.println(map.get("Неля"));
        System.out.println(map.get("Катерина"));
        System.out.println(map.get("Полина"));
        System.out.println(map.get("Регина"));


        Set<String> set = new HashSetImpl<>();

        set.add("Марсель");
        set.add("Марсель");

        set.add("Денис");
        set.add("Илья");
        set.add("Неля");
        set.add("Катерина");

        set.add("Полина");
        set.add("Полина");

        set.add("Регина");

        set.add("Максим");
        set.add("Максим");


        set.add("Сергей");
        set.add("Иван");
        set.add("Виктор");

        set.add("Виктор Александрович");

        System.out.println("set.contains(\"Марсель\") = " + set.contains("Марсель"));
        System.out.println("set.contains(\"Ноколай\") = " + set.contains("Николай"));

    }
}
