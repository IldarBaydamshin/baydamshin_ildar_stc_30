package ru.inno;

public class HashSetImpl<V> implements Set<V> {
    private static final Object NULL_OBJECT = new Object();

    private HashMapImpl<V, Object> mapForSet = new HashMapImpl<>();

    @Override
    public void add(V value) {
        mapForSet.put(value, NULL_OBJECT);
    }

    @Override
    public boolean contains(V value) {
        return mapForSet.get(value) != null;
    }
}
