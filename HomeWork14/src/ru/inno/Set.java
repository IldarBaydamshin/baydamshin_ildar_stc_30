package ru.inno;

// TODO: реализовать класс HashSet, который будет использовать HashMap -> Done
public interface Set<V> {
    void add(V value);
    boolean contains(V value);
}
