package ru.inno;

public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private MapEntry<K, V>[] entries = new MapEntry[DEFAULT_SIZE];

    // TODO: предусмотреть ЗАМЕНУ ЗНАЧЕНИЯ -> Done
    // put("Марсель", 27);
    // put("Марсель", 28);
    // у меня сохранятся оба значения, вам нужно сделать, чтобы хранилось только одно
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else {
            // если уже в этой ячейке (bucket) уже есть элемент
            // запоминяем его
            MapEntry<K, V> current = entries[index];

            // [added by Ildar]
            boolean newMapEntryKeyExist = newMapEntry.key == current.key;

            // теперь дойдем до последнего элемента в этом списке
            //while (current.next != null) { [//modified by ildar]
            while (current.next != null && newMapEntryKeyExist) {
                current = current.next;

                //[added by Ildar]
                if (newMapEntry.key == current.key) {
                    newMapEntryKeyExist = false;
                }
            }

            // теперь мы на последнем элементе, просто добавим новый элемент в конец
            // [added by Ildar] если значение ключа было новым
            if (newMapEntryKeyExist) {
                current.value = newMapEntry.value;
            } else {
                current.next = newMapEntry;
            }
        }
    }


    // TODO: реализовать -> Dode
    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1);
        MapEntry<K, V> set = entries[index];
        if (set != null) {
            if (set.key == key) {
                return set.value;
            } else {
                while (set.next != null) {
                    if (set.key == key) {
                        return set.value;
                    }
                    set = set.next;
                }
            }
        }
        return null;
    }
}
