package ru.inno.repositories;

import ru.inno.models.Game;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;
import java.util.Optional;

public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT =
            "insert into game (" +
                    "start_time, " +
                    "duration, " +
                    "player_a_id, " +
                    "player_b_id, " +
                    "number_of_shots" +
                    ")" +
                    "values (?, ?, ?, ?, ?) ";

    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void save(Game entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement =
                     connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setTimestamp (1, entity.getStartTime());
            statement.setInt(2, entity.getDuration());
            statement.setInt(3, entity.getPlayerA());
            statement.setInt(4, entity.getPlayerB());
            statement.setInt(5, entity.getNumberOfShots());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }

            ResultSet generatedIds = statement.getGeneratedKeys();

            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("game_id");
                entity.setGameId(generatedId);
            } else {
                throw new SQLException("Cannot return game_id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game entity) {

    }

    @Override
    public void delete(Integer integer) {

    }

    @Override
    public Optional<Game> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public List<Game> findAll() {
        return null;
    }

    @Override
    public List<Game> findAllByPlayerId(Integer playerId) {
        return null;
    }
}
