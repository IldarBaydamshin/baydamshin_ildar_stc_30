package ru.inno.repositories;

import ru.inno.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private final String SQL_FIND_ALL = "select * from player";

    //language=SQL
    private final String SQL_FIND_BY_NUMBER_OF_POINTS =
            "select * from player where number_of_points = ?";

    //language=SQL
    private static String SQL_FIND_BY_ID =
            "select * from player where player_id = ?";

    //language=SQL
    private static final String SQL_INSERT =
            "insert into player (" +
                    "player_name, " +
                    "last_ip_addr, " +
                    "number_of_games, " +
                    "number_of_points, " +
                    "number_of_wins," +
                    "number_of_vendettas) " +
                    "values  (?, ?, ?, ?, ?, ?);";

    //language=SQL
    private static final String SQL_UPDATE = "update player set " +
            "(player_name, last_ip_addr, number_of_games, number_of_points, number_of_wins, number_of_vendettas)" +
            " = (?, ?, ?, ?, ?, ?) " +
            "where player_id = ?";


    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private RowMapper<Player> playerRowMapper = row -> Player.builder()
            .playerId(row.getInt("player_id"))
            .playerName(row.getString("player_name"))
            .lastIpAddress(row.getString("last_ip_addr"))
            .numberOfGames(row.getInt("number_of_games"))
            .numberOfPoints(row.getInt("number_of_points"))
            .numberOfWins(row.getInt("number_of_wins"))
            .numberOfVendettas(row.getInt("number_of_vendettas"))
            .build();


    @Override
    public List<Player> findAllByPoints(Integer points) {
        List<Player> accounts = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NUMBER_OF_POINTS)) {

            statement.setInt(1, points);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Player account = playerRowMapper.mapRow(resultSet);
                accounts.add(account);
            }
            resultSet.close();
            return accounts;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, entity.getPlayerName());
            statement.setString(2, entity.getLastIpAddress());
            statement.setInt(3, entity.getNumberOfGames());
            statement.setInt(4, entity.getNumberOfPoints());
            statement.setInt(5, entity.getNumberOfWins());
            statement.setInt(6, entity.getNumberOfVendettas());

            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save entity");
            }

            ResultSet generatedIds = statement.getGeneratedKeys();

            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("player_id");
                entity.setPlayerId(generatedId);
            } else {
                throw new SQLException("Cannot return player_id");
            }

            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {
            statement.setString(1, entity.getPlayerName());
            statement.setString(2, entity.getLastIpAddress());
            statement.setInt(3, entity.getNumberOfGames());
            statement.setInt(4, entity.getNumberOfPoints());
            statement.setInt(5, entity.getNumberOfWins());
            statement.setInt(6, entity.getNumberOfVendettas());
            statement.setInt(7, entity.getPlayerId());

            int updatedRowsCount = statement.executeUpdate();

            if (updatedRowsCount == 0) {
                throw new SQLException("Cannot update entity");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(Integer id) {
        System.out.println("TBD");
    }

    @Override
    public Optional<Player> findById(Integer id) {
//        List<Player> players = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID)) {

            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return Optional.of(playerRowMapper.mapRow(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Player> findAll() {
        List<Player> players = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SQL_FIND_ALL)) {

            while (resultSet.next()) {
                Player player = playerRowMapper.mapRow(resultSet);
                players.add(player);
            }
            return players;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
