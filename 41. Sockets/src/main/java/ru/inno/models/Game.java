package ru.inno.models;

import lombok.*;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@Builder

public class Game {
    private int gameId;
    private Timestamp startTime;
    private int duration;
    private int playerA;
    private int playerB;
    private int numberOfShots;
}
