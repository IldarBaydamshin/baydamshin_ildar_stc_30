package ru.inno.models;

import lombok.*;

@Data
@AllArgsConstructor
@Builder

public class Player {
    private int playerId;
    private String playerName;
    private String lastIpAddress;
    private int numberOfGames;
    private int numberOfPoints;
    private int numberOfWins;
    private int numberOfVendettas;

}
