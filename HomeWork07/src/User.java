public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    private User(String firstName, String lastName, int age, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.isWorker = isWorker;
    }

    public static class builder {
        private String firstName = "";
        private String lastName = "";
        private int age;
        private boolean isWorker;

        public builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public builder age(int age) {
            this.age = age;
            return this;
        }

        public builder isWorker(boolean isWorker) {
            this.isWorker = isWorker;
            return this;
        }

        public User build() {
            return new User(this.firstName, this.lastName, this.age, this.isWorker);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void printUserFields() {
        System.out.println("First name: " + firstName);
        System.out.println("Last name : " + lastName);
        System.out.println("Age.......: " + age);
        System.out.println("Is Worker : " + isWorker);
    }
}
