/*
    Необходимо реализовать паттерн Builder, отрабатывающий по следующей схеме:
        User user = new User.builder()
                .firstName(“Marsel”)
                .lastName(“Sidikov”)
                .age(26)
                .isWorker(true)
                .build();
 */

public class Main {
    public static void main(String[] args) {
        User user = new User.builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .build();

        user.printUserFields(); // Вывод полей на экран
    }
}
