package texttoarray;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;

public class TextAsArray {
    File file;
    FileReader reader;
    private ArrayList<String> text;

    public TextAsArray(String fileName) throws FileNotFoundException {
        this.file = new File(fileName);
        this.reader = new FileReader(this.file);
        this.text = new ArrayList<>();
    }


    public void addTextToArray() throws IOException {
        InputStream inputStream = new FileInputStream(file);
        Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(reader);

        String line;
        String[] string;
        StringBuilder stringBuilder = new StringBuilder();

        while ((line = bufferedReader.readLine()) != null) {
            string = line.split(" ");
            for (String word : string) {
                stringBuilder.delete(0, stringBuilder.length());
                for (Character c : word.trim().toCharArray()) {
                    if (Character.isLetterOrDigit(c)) {
                        stringBuilder.append(c);
                    }
                }
                if (stringBuilder.length() != 0) {
                    text.add(stringBuilder.toString().toLowerCase());
                }
            }
        }
    }

    public void sortTextArray() {
        sortArray(text, 0, text.size() - 1);
    }

    private void sortArray(ArrayList<String> text, int begin, int end) {

        int leftIndex = begin;
        int rightIndex = end;
        int pivotIndex = (begin + end) / 2;

        while (leftIndex < rightIndex) {
            if (text.get(leftIndex).compareTo(text.get(pivotIndex)) >= 0) {
                if (text.get(rightIndex).compareTo(text.get(pivotIndex)) < 0) {
                    Collections.swap(text, leftIndex, rightIndex);
                } else {
                    rightIndex--;
                }
            } else {
                leftIndex++;
            }
        }

        if (pivotIndex > leftIndex) {
            Collections.swap(text, leftIndex, pivotIndex);
            pivotIndex = leftIndex;
        }

        if (begin < pivotIndex) {
            sortArray(text, begin, pivotIndex);
        }

        if (begin < end) {
            sortArray(text, begin + 1, end);
        }
    }

    public ArrayList<String> getTextArray() {
        return text;
    }
}