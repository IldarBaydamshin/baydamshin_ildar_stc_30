/*
Реализовать “быструю” сортировку для набора строковых данных.
На вход приложение принимает файл text.txt,
на выход - упорядоченный в лексикографическом порядке список слов текста.
 */

import texttoarray.TextAsArray;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        String fileName = "text.txt";
        TextAsArray poem = new TextAsArray(fileName);

        poem.addTextToArray();
        System.out.println(poem.getTextArray());

        poem.sortTextArray();
        System.out.println(poem.getTextArray());

    }
}

