package ru.inno;

public interface Parser {
    int parse(String string);

}
