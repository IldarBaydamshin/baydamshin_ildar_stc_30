package ru.inno;

public class ParserImpl implements Parser {
    public int parse(String string) {

        if (string == null || string.equals("")) {
            throw new IllegalArgumentException();
        }

        int result = 0;
        char number[] = string.toCharArray();
        int mult = 1;



        for (int i = number.length -1; i >= 0; i--) {
            result = result + (number[i] - '0') * mult;
            mult *= 10;
        }
        return result;
    }
}
