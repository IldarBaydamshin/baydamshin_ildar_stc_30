package ru.inno;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParserImplTest {
    private ParserImpl parser;

    @BeforeEach
    public void setUp() {
        System.out.println(" Создан новый экземпляр класса ParserImpl");
        parser = new ParserImpl();
    }

    @Test
    public void testOnCorrectNumber() {
        String forTest = "12345";
        int expected = 12345;

        int actual = parser.parse(forTest);

        assertEquals(expected, actual);
    }
}